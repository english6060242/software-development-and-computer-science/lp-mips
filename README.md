# Computer Architecture - Segmented MIPS

## Task and Requirements

- **Implementation of Segmented MIPS Processor**: The processor should be implemented in the following stages:
  - IF (Instruction Fetch): Fetching the instruction from the program memory.
  - ID (Instruction Decode): Decoding the instruction and reading registers.
  - EX (Execute): Execution of the instruction.
  - MEM (Memory Access): Reading from or writing to the data memory.
  - WB (Write Back): Writing results to the registers.

- **Support for Hazards**: The processor must support the following types of hazards:
  - Structural: Occur when two instructions attempt to use the same resource in the same cycle.
  - Data: Attempting to use data before it is ready. Maintaining strict order of reads and writes.
  - Control: Trying to make a decision about a condition that has not been evaluated yet. To support these hazards, a short-circuit unit and a hazard detection unit must be implemented.

- **Instructions to Implement**: The following instructions must be implemented:
  - R-Type: SLL, SRL, SRA, SLLV, SRLV, SRAV, ADDU, SUBU, AND, OR, XOR, NOR, SLT.
  - I-Type: LB, LH, LW, LWU, LBU, LHU, SB, SH, SW, ADDI, ANDI, ORI, XORI, LUI, SLTI, BEQ, BNE, J, JAL.
  - J-Type: JR, JALR.

- **Program Assembly and Loading into Memory**: The program to be executed must be loaded into the program memory using an assembled file. The following tasks should be performed:
  - Implementation of an assembler program that converts MIPS assembly code to instruction code.
  - Transmission of this program via UART before starting execution.

- **Debug Unit**: A debug unit must be simulated to send information to and from the processor via UART. The information to be sent via UART includes:
  - Contents of the 32 registers.
  - PC.
  - Contents of the used data memory.
  - Number of clock cycles since the start.

- **Operating Modes**: The processor must allow two operating modes:
  - Continuous: A command is sent to the FPGA via UART, and it initiates the program execution until reaching the end (HALT instruction). At that point, all indicated values are displayed on the screen.
  - Step by Step: By sending a command via UART, one clock cycle is executed. The indicated values must be shown at each step.

- **Development Validation**: Present the simulated work and validate the development through Test Bench.

- **Post-Synthesis Simulation**: The simulation must be post-synthesis and include timing information. The following steps must be followed:
  - The system clock must be created using the corresponding ip-core.
  - Show timing report, with maximum supported clock frequency.

## Clone Repository

```bash
cd existing_repo
git remote add origin https://gitlab.com/english6060242/software-development-and-computer-science/lp-mips.git
git branch -M main
git pull origin main
```

## Simulation Preparation

Before simulating the processor, ensure to perform the following steps:

1. **Import Simulation Files**: The `.mem` files must be imported as simulation sources. These files contain the program in machine language, the number of lines of the program, and the final output of the processor. The files are:
   - `Code.mem`: Contains the program in machine language, sent to the processor line by line via UART.
   - `CLP.mem`: Contains the number of lines the program has, used by the state machine that controls the UART module.
   - `Output.mem`: Contains the final value of PC, the number of clocks used, the values of the first 5 registers, and the first 5 positions of the data memory.

2. **Clocking Module Generation**: It is necessary to generate the `clk_wiz_0` module using Clocking Wizard.

## Project Modules

The project consists of the following modules:

- **Top**: It is the main result of the project, which would be loaded into the FPGA.
- **Debug_Interface**: Counterpart of the Debug_Unit module contained in Top. It facilitates communication between the program and the data once the program has been executed.
- **Isolated_MIPS**: MIPS version without the modules to load the program or control the operating mode. Used for testing in shorter simulations.
- **Non_CLK_Top** and **Non_CLK_Debug_Interface**: Versions of Top and Debug_Interface without the module generated by Clocking Wizard. Used to simulate the complete system in purely behavioral mode.

## Changing Operating Mode

To change the operating mode, follow these steps:

1. Modify the `Mode_Op` variable in `Non_CLK_Debug_Testbench`.
   - `Mode_Op = 0`: Continuous Mode.
   - `Mode_OP = 1`: Step by Step Mode.

### For explanations on the operation of the complete system, simulation results, and timing analysis, refer to the report.
