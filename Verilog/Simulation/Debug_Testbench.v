`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01.06.2022 10:30:27
// Design Name: 
// Module Name: Debug_Testbench
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Debug_Testbench
#(
    parameter   NB_DATA = 32,
    parameter   NB_UART = 8,    // Number of data bits
    parameter   SB_TICK = 16,
    parameter   NB_CONT = 4,
    parameter   NB_BRCV = 3,
    parameter   M = 14,        // Parameter for Baud Rate Generator
    parameter   W = 8,          // # buffer of W bits
    parameter   OP_BITS = 6,    // Operation bits
    parameter   NB_STAT = 4,    // Number of bits to represent states
    parameter   NB_SEND = 384, // Number of bits to send (table)
    parameter   TC_WORD = 12,    // Number of 32-bit words in the table
    parameter   NB_DIR_PC   = 32,
    parameter   NB_ALUOP    = 4,
    parameter   NB_SHAMT    = 5,
    parameter   NB_OPCODE   = 6,
    parameter   NB_FUNC     = 6,
    parameter   NB_RDIR     = 5,     // Register addressing bits
    parameter   NB_MUX4     = 2,
    parameter   NB_ALLR     = 160,  // Number of bits of registers to send
    parameter   NB_DMEM     = 160    // Number of bits of data memory to send
)
();

    reg                                     clk;
    //reg                                     clk2;
    reg                                     Reset;
    reg                                     Reset2;
    reg                                     Reset_clk;
    wire                                    locked;
    wire                                    locked2;
    reg            [NB_DATA  - 1 : 0]       CLP_MEM [1 : 0]; // Number of program lines
    
    reg            [NB_UART  - 1 : 0]       Mode_Op; 
    
    wire           [NB_DATA  - 1 : 0]       CLP; 
    //wire           [NB_STAT  - 1 : 0]       Debug_State;
    wire                                    Write_Hab;
    wire           [NB_SEND  - 1 : 0]       Table;
    //wire           [NB_DATA  - 1 : 0]       Prog_Cont;
    
    wire                                    hlt;
    wire           [NB_SEND  - 1 : 0]       w_table;     
    
    assign CLP = CLP_MEM[0];
    
    initial
    begin
        #0
        $readmemb("C:\\Users\\Lucas\\Desktop\\Arqui\\Code\\TPF-RC11\\CLP.mem",CLP_MEM);
        clk      = 1'b0;
        //clk2      = 1'b0;
        Mode_Op = 32'b0;
        //Reset    = 1'b1;
        Reset_clk   = 1'b1;
        #25
        Reset_clk   = 1'b0;
        //#1
        //Reset    = 1'b0;
    end
    
    always #5 clk  = ~clk;  // 10 ns on, 10 ns off = 50MHz 
    
    //always #5 clk2  = ~clk2; // 5 ns on, 5 ns off = 100MHz
    
    always@(*)
    begin
        if(locked == 0)
        begin
            Reset = 1'b1;
        end
        else 
        begin
            Reset = 1'b0;
        end
    end
    
    always@(*)
    begin
        if(locked2 == 0)
        begin
            Reset2 = 1'b1;
        end
        else 
        begin
            Reset2 = 1'b0;
        end
    end

Top
#(
    .NB_DATA                (NB_DATA),
    .NB_UART                (NB_UART),    // Number of data bits
    .SB_TICK                (SB_TICK),
    .NB_CONT                (NB_CONT),
    .NB_BRCV                (NB_BRCV),
    .M                      (M),        // Parameter for Baud Rate Generator
    .W                      (W),          // # buffer of W bits
    .OP_BITS                (OP_BITS),    // Operation bits
    .NB_STAT                (NB_STAT),    // Number of bits to represent states
    .NB_SEND                (NB_SEND), // Number of bits to send (table)
    .TC_WORD                (TC_WORD),    // Number of 32-bit words in the table
    .NB_DIR_PC              (NB_DIR_PC),
    .NB_ALUOP               (NB_ALUOP),
    .NB_SHAMT               (NB_SHAMT),
    .NB_OPCODE              (NB_OPCODE),
    .NB_FUNC                (NB_FUNC),
    .NB_RDIR                (NB_RDIR),     // Register addressing bits
    .NB_MUX4                (NB_MUX4),
    .NB_ALLR                (NB_ALLR),  // Number of bits of registers to send
    .NB_DMEM                (NB_DMEM)// Number of bits of data memory to send
 )
u_Top_1
(
    .i_clk                  (clk),                 
    .i_Reset                (Reset),
    .i_Reset_clk            (Reset_clk),                
    .i_rx                   (MIPS_rx),
   
    .o_tx                   (MIPS_tx),
    .o_locked               (locked)
);

Debug_Interface
#(
    .NB_DATA                (NB_DATA),
    .NB_UART                (NB_UART),    // Number of data bits
    .W                      (W),          // # buffer of W bits
    .OP_BITS                (OP_BITS),    // Operation bits
    .NB_STAT                (NB_STAT),    // Number of bits to represent states
    .NB_SEND                (NB_SEND),    // Number of bits to send (table)
    .TC_WORD                (TC_WORD),    // Number of 32-bit words in the table
    .SB_TICK                (SB_TICK),
    .NB_CONT                (NB_CONT),
    .NB_BRCV                (NB_BRCV),
    .M                      (M)           // Parameter for Baud Rate Generator
)
u_Debug_Interface_1
(
    .i_clk                  (clk),
    .i_Reset                (Reset2),
    .i_Reset_clk            (Reset_clk), 
    .i_CLP                  (CLP),            // Number of program lines
    .i_Mode_Op              (Mode_Op),
   
    .i_rx                   (MIPS_tx),    
    .o_tx                   (MIPS_rx),
   
    //.o_Debug_State          (Debug_State),
    .o_Write_Hab            (Write_Hab),
    .o_Table                (Table),
    .o_locked               (locked2)      
);


endmodule