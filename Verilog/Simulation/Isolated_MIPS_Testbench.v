`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01.06.2022 11:48:06
// Design Name: 
// Module Name: Isolated_MIPS_Testbench
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Isolated_MIPS_Testbench
#(
    parameter   NB_DATA     = 32,
    parameter   NB_DIR_PC   = 32,
    parameter   NB_ALUOP    = 4,
    parameter   NB_SHAMT    = 5,
    parameter   NB_OPCODE   = 6,
    parameter   NB_FUNC     = 6,
    parameter   NB_RDIR     = 5,     // Register addressing bits
    parameter   NB_MUX4     = 2,
    parameter   NB_SEND     = 1408, // Number of bits to send (table)
    parameter   NB_ALLR     = 1024,  // Number of bits of registers to send
    parameter   NB_DMEM     = 320    // Number of bits of data memory to send   
 )
();

    reg                                 clk;
    reg                                 Reset;
    reg                                 Reset_clk;
    wire                                locked;
    reg                                 Debug_Stall;
    wire                                hlt;
    wire           [NB_SEND - 1 : 0]    w_table;
    
    initial
    begin
        #0
        clk      = 1'b0;
        Debug_Stall = 1'b0;
        Reset    = 1'b1;
        Reset_clk   = 1'b1;
        #5
        Reset_clk   = 1'b0;
        #1
        Reset    = 1'b0;
        
    end
    
    always #5 clk  = ~clk;  // 10 ns on, 10 ns off = 50MHz 
    
    always@(*)
    begin
        if(locked == 0)
        begin
            Reset = 1'b1;
        end
        else 
        begin
            Reset = 1'b0;
        end
    end

Isolated_MIPS
#(
    .NB_DATA                (NB_DATA),
    .NB_DIR_PC              (NB_DIR_PC),
    .NB_ALUOP               (NB_ALUOP),
    .NB_SHAMT               (NB_SHAMT),
    .NB_OPCODE              (NB_OPCODE),
    .NB_FUNC                (NB_FUNC),
    .NB_RDIR                (NB_RDIR),     // Register addressing bits
    .NB_MUX4                (NB_MUX4),
    .NB_SEND                (NB_SEND),     // Number of bits to send (table)
    .NB_ALLR                (NB_ALLR),     // Number of bits of registers to send
    .NB_DMEM                (NB_DMEM)      // Number of bits of data memory to send
 )
u_Isolated_MIPS_1 
(
    // Inputs
    .i_clk                  (clk),
    .i_Reset_clk            (Reset_clk),
    .i_Reset                (Reset),                // Reset
    .i_Stall                (Debug_Stall),   
    .i_Prog_Write_Address   (32'b0),
    .i_Prog_MEM_Data        (8'b0),
    .i_Prog_MEM_Write       (0),
    // Outputs
    .o_HLT                  (hlt),
    .o_locked               (locked),
    .o_table                (w_table)
 );

endmodule
