`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01.06.2022 12:38:45
// Design Name: 
// Module Name: Non_CLK_Debug_Testbench
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Non_CLK_Debug_Testbench 
#(
    parameter   NB_DATA = 32,
    parameter   NB_UART = 8,    // Number of data bits
    parameter   SB_TICK = 16,
    parameter   NB_CONT = 4,
    parameter   NB_BRCV = 3,
    parameter   M = 14,        // Parameter for Baud Rate Generator
    parameter   W = 8,          // # buffer of W bits
    parameter   OP_BITS = 6,    // Operation bits
    parameter   NB_STAT = 4,    // Number of bits to represent states
    parameter   NB_SEND = 384, // Number of bits to send (table)
    parameter   TC_WORD = 12,    // Number of 32-bit words in the table
    parameter   NB_DIR_PC   = 32,
    parameter   NB_ALUOP    = 4,
    parameter   NB_SHAMT    = 5,
    //parameter   NB_DECO     = 3,
    parameter   NB_OPCODE   = 6,
    parameter   NB_FUNC     = 6,
    parameter   NB_RDIR     = 5,     // Register addressing bits
    parameter   NB_MUX4     = 2,
    parameter   NB_ALLR     = 160,  // Number of register bits to send
    parameter   NB_DMEM     = 160    // Number of data memory bits to send
)
();

    reg                                     clk;
    reg                                     Reset;
    reg            [NB_DATA  - 1 : 0]       CLP_MEM [1 : 0]; // Number of program lines
    
    reg            [NB_UART  - 1 : 0]       Mode_Op; 
    
    wire           [NB_DATA  - 1 : 0]       CLP; 
    wire                                    Write_Hab;
    wire           [NB_SEND  - 1 : 0]       Table;
    
    assign CLP = CLP_MEM[0];
    
    initial
    begin
        #0
        $readmemb("C:\\Users\\Lucas\\Desktop\\Arqui\\Code\\TPF-RC11\\CLP.mem",CLP_MEM);
        clk      = 1'b0;
        Mode_Op = 32'b0;
        Reset    = 1'b1;
        #25
        Reset    = 1'b0;
        
    end
    
    always #10 clk  = ~clk;  // 10 ns on, 10 ns off = 50MHz 

Non_CLK_Top
#(
    .NB_DATA                (NB_DATA),
    .NB_UART                (NB_UART),    // Number of data bits
    .SB_TICK                (SB_TICK),
    .NB_CONT                (NB_CONT),
    .NB_BRCV                (NB_BRCV),
    .M                      (M),        // Parameter for Baud Rate Generator
    .W                      (W),          // # buffer of W bits
    .OP_BITS                (OP_BITS),    // Operation bits
    .NB_STAT                (NB_STAT),    // Number of bits to represent states
    .NB_SEND                (NB_SEND), // Number of bits to send (table)
    .TC_WORD                (TC_WORD),    // Number of 32-bit words in the table
    .NB_DIR_PC              (NB_DIR_PC),
    .NB_ALUOP               (NB_ALUOP),
    .NB_SHAMT               (NB_SHAMT),
    .NB_OPCODE              (NB_OPCODE),
    .NB_FUNC                (NB_FUNC),
    .NB_RDIR                (NB_RDIR),     // Register addressing bits
    .NB_MUX4                (NB_MUX4),
    .NB_ALLR                (NB_ALLR),  // Number of register bits to send
    .NB_DMEM                (NB_DMEM)// Number of data memory bits to send
 )
u_Top_1
(
    .i_clk                  (clk),                  // Clock
    .i_Reset                (Reset),                // Reset  
    .i_rx                   (MIPS_rx),
   
    .o_tx                   (MIPS_tx)
);

Non_CLK_Debug_Interface
#(
    .NB_DATA                (NB_DATA),
    .NB_UART                (NB_UART),    // Number of data bits
    .W                      (W),          // # buffer of W bits
    .OP_BITS                (OP_BITS),    // Operation bits
    .NB_STAT                (NB_STAT),    // Number of bits to represent states
    .NB_SEND                (NB_SEND),    // Number of bits to send (table)
    .TC_WORD                (TC_WORD),    // Number of 32-bit words in the table
    .SB_TICK                (SB_TICK),
    .NB_CONT                (NB_CONT),
    .NB_BRCV                (NB_BRCV),
    .M                      (M)           // Parameter for Baud Rate Generator
)
u_Debug_Interface_1
(
    .i_clk                  (clk),
    .i_Reset                (Reset),
    .i_CLP                  (CLP),            // Number of program lines
    .i_Mode_Op              (Mode_Op),
   
    .i_rx                   (MIPS_tx),    
    .o_tx                   (MIPS_rx),
   
    .o_Write_Hab            (Write_Hab),
    .o_Table                (Table)    
);


endmodule
