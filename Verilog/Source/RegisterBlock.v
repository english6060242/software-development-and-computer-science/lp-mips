`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01.06.2022 09:10:32
// Design Name: 
// Module Name: RegisterBlock
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module RegisterBlock
#(
    parameter   NB_DATA = 32,
    parameter   NB_RDIR = 5,
    parameter   NB_ALLR = 160 
 )
(
    // Inputs
    input   wire                        i_clk,                  // Clock
    input   wire                        i_Reset,                // Reset
    input   wire    [NB_RDIR - 1 : 0]   i_Src_Register_Out_1,   // Addressing for output 1
    input   wire    [NB_RDIR - 1 : 0]   i_Src_Register_Out_2,   // Addressing for output 2
    input   wire    [NB_RDIR - 1 : 0]   i_Dest_Register,        // Addressing for writing
    input   wire    [NB_DATA - 1 : 0]   i_Data_In,              // Data for writing
    input   wire                        i_Write_Reg,            // Write signal
    
    // Outputs
    output  wire    [NB_DATA - 1 : 0]   o_Read_Data_1,
    output  wire    [NB_DATA - 1 : 0]   o_Read_Data_2,
    output  wire    [NB_ALLR - 1 : 0]   o_All_Registers
 );
 
 // Internal Registers
 reg   [NB_DATA - 1 : 0]   Registers [0 : NB_DATA - 1];    // Actual registers
 reg   [NB_DATA - 1 : 0]   Read_Data_1;
 reg   [NB_DATA - 1 : 0]   Read_Data_2;
 
genvar i;        
generate 
    for(i = 0; i < 4 ; i = i +1)
    begin
        assign o_All_Registers[NB_DATA - 1 + NB_DATA * i : 0 + NB_DATA * i] = Registers[i];
    end
endgenerate
 
assign o_All_Registers[NB_DATA - 1 + NB_DATA * 4 : 0 + NB_DATA * 4] = Registers[31];
 
assign    o_Read_Data_1 = Read_Data_1;
assign    o_Read_Data_2 = Read_Data_2;
 
 // Read Registers / Reset
always @(negedge i_clk) 
     begin
        if(i_Reset)
            begin
                Read_Data_1 <= {NB_DATA{1'b0}};
                Read_Data_2 <= {NB_DATA{1'b0}};
            end
        else
            begin
                Read_Data_1 <= Registers[i_Src_Register_Out_1];
                Read_Data_2 <= Registers[i_Src_Register_Out_2];
                //$display("Reading register %d with %d",i_Src_Register_Out_1,Registers[i_Src_Register_Out_1]);
                //$display("Reading register %d with %d",i_Src_Register_Out_2,Registers[i_Src_Register_Out_2]);
            end    
     end
 
 // Write
 always @(posedge i_clk)
     begin
        if(i_Write_Reg && (!(i_Reset)))
            begin
                Registers[i_Dest_Register] <= i_Data_In;
                $display("Writing register %d with %d",i_Dest_Register,i_Data_In);
            end
     end

endmodule
