`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01.06.2022 09:14:04
// Design Name: 
// Module Name: Data_Memory
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Data_Memory
#(
    parameter   NB_DATA      = 32,
    parameter   NB_BYTE      = 8,
    parameter   CANT_DIR_MEM = 1024,
    parameter   NB_DMEM      = 160
)
(
    // Inputs
    input   wire                        i_clk,                  // Clock
    input   wire                        i_Reset,                // Reset
    input   wire                        i_Data_Mem_Read,
    input   wire                        i_Data_Mem_Write,
    input   wire                        i_Extend_Sign, 
    input   wire    [1           : 0]   i_BHW,
    input   wire    [NB_DATA - 1 : 0]   i_Address,
    input   wire    [NB_DATA - 1 : 0]   i_Write_Data,
    
    // Outputs
    output  wire    [NB_DATA - 1 : 0]   o_Read_Data,
    output  wire    [NB_DMEM - 1 : 0]   o_Data_MEM_Block
    );

    // Internal Registers
    reg     [NB_BYTE - 1 : 0] Memory_0 [0 : CANT_DIR_MEM - 1];  // Memory_0 - Memory_3 : Memory
    reg     [NB_BYTE - 1 : 0] Memory_1 [0 : CANT_DIR_MEM - 1]; 
    reg     [NB_BYTE - 1 : 0] Memory_2 [0 : CANT_DIR_MEM - 1]; 
    reg     [NB_BYTE - 1 : 0] Memory_3 [0 : CANT_DIR_MEM - 1]; 
    
    reg     [NB_BYTE - 1 : 0] tmp_read_0;                       // tmp_read_0 - tmp_read_3 - Output Registers
    reg     [NB_BYTE - 1 : 0] tmp_read_1;  
    reg     [NB_BYTE - 1 : 0] tmp_read_2;  
    reg     [NB_BYTE - 1 : 0] tmp_read_3;    
    
    genvar i;
    generate
       for(i = 0; i < 5; i = i + 1)
       begin
           assign o_Data_MEM_Block[NB_DATA - 1 + NB_DATA * i : 0 + NB_DATA * i] = {Memory_3[i],Memory_2[i],Memory_1[i],Memory_0[i]};
       end
    endgenerate
    
    assign o_Read_Data = {tmp_read_3, tmp_read_2, tmp_read_1, tmp_read_0};
    
    // Write
    always @(negedge i_clk)   
    begin
        if(i_Data_Mem_Write)
        begin
            if(i_BHW == 2'b10)
            begin
                Memory_0[i_Address>>2] <= i_Write_Data[7 : 0];
                Memory_1[i_Address>>2] <= i_Write_Data[15 : 8];
                Memory_2[i_Address>>2] <= i_Write_Data[23 : 16];
                Memory_3[i_Address>>2] <= i_Write_Data[31 : 24];
                $display("Writing to data[%d] with %d",i_Address,i_Write_Data);
            end
            else if(i_BHW == 2'b01)
            begin
                case(i_Address [1])
                    0:
                    begin
                        Memory_0[i_Address>>2] <= i_Write_Data[7 : 0];
                        Memory_1[i_Address>>2] <= i_Write_Data[15 : 8];
                        $display("Writing to data[%d] with %d",i_Address,i_Write_Data[15 : 0]);
                    end
                    1:
                    begin
                        Memory_2[i_Address>>2] <= i_Write_Data[7 : 0];
                        Memory_3[i_Address>>2] <= i_Write_Data[15 : 8];
                        $display("Writing to data[%d] with %d",i_Address,i_Write_Data[15 : 0]);
                    end
                endcase
            end
            else if (i_BHW == 2'b00)
            begin
                case(i_Address [1 : 0])
                    0: 
                    begin
                        Memory_0[i_Address>>2] <= i_Write_Data[7 : 0];
                        $display("Writing to data[%d] with %d",i_Address,i_Write_Data[7 : 0]);
                    end
                    1: 
                    begin
                        Memory_1[i_Address>>2] <= i_Write_Data[7 : 0];
                        $display("Writing to data[%d] with %d",i_Address,i_Write_Data[7 : 0]);
                    end
                    2: 
                    begin
                        Memory_2[i_Address>>2] <= i_Write_Data[7 : 0];
                        $display("Writing to data[%d] with %d",i_Address,i_Write_Data[7 : 0]);
                    end
                    3: 
                    begin
                        Memory_3[i_Address>>2] <= i_Write_Data[7 : 0];
                        $display("Writing to data[%d] with %d",i_Address,i_Write_Data[7 : 0]);
                    end
                endcase            
            end
        end
    end
    
    // Read
    always @(posedge i_clk)   
    begin 
        if(i_Reset)
        begin
            tmp_read_0 <= 8'b0;
            tmp_read_1 <= 8'b0;
            tmp_read_2 <= 8'b0;
            tmp_read_3 <= 8'b0;
        end
        if(i_Data_Mem_Read)
        begin
            case(i_BHW)
                0:
                begin
                    case(i_Address[1 : 0])
                        0:
                        begin
                            if(i_Extend_Sign == 0)
                            begin
                                tmp_read_0 <= Memory_0[i_Address>>2];
                                tmp_read_1 <= 8'b0;
                                tmp_read_2 <= 8'b0;
                                tmp_read_3 <= 8'b0;
                            end
                            else
                            begin
                                tmp_read_0 <= Memory_0[i_Address>>2];
                                tmp_read_1 <= {8{Memory_0[i_Address>>2][7]}};
                                tmp_read_2 <= {8{Memory_0[i_Address>>2][7]}};
                                tmp_read_3 <= {8{Memory_0[i_Address>>2][7]}};
                            end
                        end
                        1:
                        begin
                            if(i_Extend_Sign == 0)
                            begin
                                tmp_read_0 <= Memory_1[i_Address>>2];
                                tmp_read_1 <= 8'b0;
                                tmp_read_2 <= 8'b0;
                                tmp_read_3 <= 8'b0;
                            end
                            else
                            begin
                                tmp_read_0 <= Memory_1[i_Address>>2];
                                tmp_read_1 <= {8{Memory_1[i_Address>>2][7]}};
                                tmp_read_2 <= {8{Memory_1[i_Address>>2][7]}};
                                tmp_read_3 <= {8{Memory_1[i_Address>>2][7]}};
                            end
                        end
                        2:
                        begin
                            if(i_Extend_Sign == 0)
                            begin
                                tmp_read_0 <= Memory_2[i_Address>>2];
                                tmp_read_1 <= 8'b0;
                                tmp_read_2 <= 8'b0;
                                tmp_read_3 <= 8'b0;
                            end
                            else
                            begin
                                tmp_read_0 <= Memory_2[i_Address>>2];
                                tmp_read_1 <= {8{Memory_2[i_Address>>2][7]}};
                                tmp_read_2 <= {8{Memory_2[i_Address>>2][7]}};
                                tmp_read_3 <= {8{Memory_2[i_Address>>2][7]}};
                            end
                        end
                        3:
                        begin
                            if(i_Extend_Sign == 0)
                            begin
                                tmp_read_0 <= Memory_3[i_Address>>2];
                                tmp_read_1 <= 8'b0;
                                tmp_read_2 <= 8'b0;
                                tmp_read_3 <= 8'b0;
                            end
                            else
                            begin
                                tmp_read_0 <= Memory_3[i_Address>>2];
                                tmp_read_1 <= {8{Memory_3[i_Address>>2][7]}};
                                tmp_read_2 <= {8{Memory_3[i_Address>>2][7]}};
                                tmp_read_3 <= {8{Memory_3[i_Address>>2][7]}};
                            end
                        end
                    endcase                    
                end
                1:
                begin
                    case(i_Address[1])
                        0:
                        begin
                            if(i_Extend_Sign == 0)
                            begin
                                tmp_read_0 <= Memory_0[i_Address>>2];
                                tmp_read_1 <= Memory_1[i_Address>>2];
                                tmp_read_2 <= 8'b0;
                                tmp_read_3 <= 8'b0;
                            end
                            else
                            begin
                                tmp_read_0 <= Memory_0[i_Address>>2];
                                tmp_read_1 <= Memory_1[i_Address>>2];
                                tmp_read_2 <= {8{Memory_1[i_Address>>2][7]}};
                                tmp_read_3 <= {8{Memory_1[i_Address>>2][7]}};
                            end                        
                        end
                        1:
                        begin
                            if(i_Extend_Sign == 0)
                            begin
                                tmp_read_0 <= Memory_2[i_Address>>2];
                                tmp_read_1 <= Memory_3[i_Address>>2];
                                tmp_read_2 <= 8'b0;
                                tmp_read_3 <= 8'b0;
                            end
                            else
                            begin
                                tmp_read_0 <= Memory_2[i_Address>>2];
                                tmp_read_1 <= Memory_3[i_Address>>2];
                                tmp_read_2 <= {8{Memory_3[i_Address>>2][7]}};
                                tmp_read_3 <= {8{Memory_3[i_Address>>2][7]}};
                            end                        
                        end
                    endcase
                end
                2:
                begin
                    tmp_read_0 <= Memory_0[i_Address>>2];
                    tmp_read_1 <= Memory_1[i_Address>>2];
                    tmp_read_2 <= Memory_2[i_Address>>2];
                    tmp_read_3 <= Memory_3[i_Address>>2];
                end
                default:
                begin
                    tmp_read_0 <= Memory_0[i_Address>>2];
                    tmp_read_1 <= Memory_1[i_Address>>2];
                    tmp_read_2 <= Memory_2[i_Address>>2];
                    tmp_read_3 <= Memory_3[i_Address>>2];
                end
            endcase 
        end
    end

endmodule
