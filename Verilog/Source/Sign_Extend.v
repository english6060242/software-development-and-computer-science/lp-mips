`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01.06.2022 09:10:54
// Design Name: 
// Module Name: Sign_Extend
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Sign_Extend
#(
    parameter   NB_DATA_IN = 16,
    parameter   NB_DATA_OUT = 32
)
(
    // Inputs
    input   wire    [NB_DATA_IN - 1 : 0]    i_SE_In,
    // Outputs
    output  wire    [NB_DATA_OUT - 1 : 0]   o_SE_Out
);
 
localparam  DIF = NB_DATA_OUT - NB_DATA_IN;

reg                         r_last_bit;
reg     [NB_DATA_IN - 2 : 0]  r_resto;

always @(*)
begin
    {r_last_bit, r_resto} = i_SE_In;
end

assign  o_SE_Out = {{DIF{r_last_bit}},i_SE_In}; // Concatenate the input with the 
                                                // last input bit repeated DIF times

endmodule
