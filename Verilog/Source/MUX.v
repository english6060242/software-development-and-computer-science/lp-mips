`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01.06.2022 09:09:51
// Design Name: 
// Module Name: MUX
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module MUX
#(
    parameter   NB_DATA = 32
 )
 (
    // Inputs
    input   wire    [NB_DATA - 1 : 0]    i_MUX_0,
    input   wire    [NB_DATA - 1 : 0]    i_MUX_1,
    input   wire                         i_MUX_Select,
    // Outputs
    output  wire    [NB_DATA - 1 : 0]    o_MUX
 );
 
 assign o_MUX = i_MUX_Select ? i_MUX_1 : i_MUX_0;

endmodule

