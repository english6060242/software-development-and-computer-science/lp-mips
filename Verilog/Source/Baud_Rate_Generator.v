`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01.06.2022 09:14:54
// Design Name: 
// Module Name: Baud_Rate_Generator
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Baud_Rate_Generator
#(
    parameter   M = 14 //326   14 -> 230400
 )
 (
    // Inputs
    input   wire                           i_clk,
    input   wire                           i_Reset,

    // Outputs
    output  wire                            s_tick
 );
 
	// Opcode
	localparam N = log2(M);
	
	reg    [N        - 1 : 0]   r_reg;
	wire   [N        - 1 : 0]   r_next;

    always@(posedge i_clk, posedge i_Reset)
    begin
        if(i_Reset)
        begin
            r_reg <= 0;
        end
        else
        begin
            r_reg <= r_next;
        end
    end
    
    assign r_next = (r_reg == (M - 1)) ? 0 : r_reg + 1;
    
    assign s_tick = (r_reg == (M - 1)) ? 1'b1 : 1'b0;
    
    function integer log2(input integer n);
        integer i;
        begin
            log2 = i;
            for(i = 0; 2**i < n; i = i + 1)
            begin
                log2 = i + 1;
            end
        end
    endfunction 
       
endmodule
