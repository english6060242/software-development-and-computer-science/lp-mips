`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01.06.2022 09:14:20
// Design Name: 
// Module Name: MEM_WB
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module MEM_WB
#(
    parameter   NB_DATA  = 32,
    parameter   NB_RDIR  = 5,
    parameter   NB_ALUOP = 4,
    parameter   NB_MUX4  = 2
 )
(
    // clk and reset
    input   wire                         i_clk,                  // Clock
    input   wire                         i_Reset,                // Reset
    
    // Store data from EX stage to be used in MEM stage
    input   wire    [NB_DATA  - 1 : 0]   i_ALU_Result_MEM,       // ALU Result
    input   wire    [NB_DATA  - 1 : 0]   i_Mem_Data_Out_MEM,     // Data to write into memory from register file
    input   wire    [NB_DATA  - 1 : 0]   i_PC_Plus_Four_MEM,    // PC + 4
    
    input   wire    [NB_RDIR  - 1 : 0]   i_rs_MEM,
    input   wire    [NB_RDIR  - 1 : 0]   i_rt_MEM,
    input   wire    [NB_RDIR  - 1 : 0]   i_rd_MEM,
    input   wire    [NB_RDIR  - 1 : 0]   i_Dest_Register_MEM,    // Destination Register
    
    // Control signals to be passed from EX stage to WB stage
    input   wire    [NB_MUX4  - 1 : 0]   i_Mem_To_Reg_MEM,           // Selects data to be written into register
    input   wire                         i_Write_Reg_MEM,            // Signal to write registers 
    
    // Outputs
    // Store data from EX stage to be used in WB stage
    output  wire    [NB_DATA  - 1 : 0]   o_ALU_Result_WB,       // ALU Result
    output  wire    [NB_DATA  - 1 : 0]   o_Mem_Data_Out_WB,
    output  wire    [NB_DATA  - 1 : 0]   o_PC_Plus_Four_WB,
    
    output  wire    [NB_RDIR  - 1 : 0]   o_rs_WB,
    output  wire    [NB_RDIR  - 1 : 0]   o_rt_WB,
    output  wire    [NB_RDIR  - 1 : 0]   o_rd_WB,
    output  wire    [NB_RDIR  - 1 : 0]   o_Dest_Register_WB,    // Destination Register
    
    // Control signals to be passed from EX stage to WB stage
    output  wire    [NB_MUX4  - 1 : 0]   o_Mem_To_Reg_WB,           // Selects data to be written into register
    output  wire                         o_Write_Reg_WB            // Signal to write registers 
        
 );  
 
    // Store data from EX stage to be used in MEM stage
    reg    [NB_DATA  - 1 : 0]   ALU_Result;
    reg    [NB_DATA  - 1 : 0]   Mem_Data_Out;
    reg    [NB_DATA  - 1 : 0]   PC_Plus_Four;
    
    // Control signals to be passed from EX stage to WB stage
    reg    [NB_MUX4  - 1 : 0]   Mem_To_Reg;           // Selects data to be written into register
    reg                         Write_Reg;            // Signal to write registers 
    
    // Signals for hazard detection 
    reg    [NB_RDIR  - 1 : 0]   rs;
    reg    [NB_RDIR  - 1 : 0]   rt;
    reg    [NB_RDIR  - 1 : 0]   rd;
    reg    [NB_RDIR  - 1 : 0]   Dest_Register;
    
    // Store data from EX stage to be used in WB stage
    assign o_ALU_Result_WB           = ALU_Result;
    assign o_Mem_Data_Out_WB         = Mem_Data_Out;
    assign o_PC_Plus_Four_WB         = PC_Plus_Four;
    
    assign  o_rs_WB                  = rs;
    assign  o_rt_WB                  = rt;
    assign  o_rd_WB                  = rd;
    assign  o_Dest_Register_WB       = Dest_Register;
    
    // Control signals to be passed from EX stage to WB stage
    assign o_Mem_To_Reg_WB           = Mem_To_Reg;           // Selects data to be written into register
    assign o_Write_Reg_WB            = Write_Reg;            // Signal to write registers
    
    always @(negedge i_clk)
    begin
        if(i_Reset)
        begin
            ALU_Result          <= 32'b0;
            Mem_Data_Out        <= 32'b0;
            PC_Plus_Four        <= 32'b0;
                  
            rs                  <= 5'b0;
            rt                  <= 5'b0;
            rd                  <= 5'b0;
            Dest_Register       <= 5'b0;              
                                
            Mem_To_Reg          <= 2'b0;    
            Write_Reg           <= 1'b0;       
        end
        else 
        begin
            ALU_Result          <= i_ALU_Result_MEM;
            Mem_Data_Out        <= i_Mem_Data_Out_MEM;
            PC_Plus_Four        <= i_PC_Plus_Four_MEM;
 
            rs                  <= i_rs_MEM;
            rt                  <= i_rt_MEM;
            rd                  <= i_rd_MEM;
            Dest_Register       <= i_Dest_Register_MEM;  
                                                     
            Mem_To_Reg          <= i_Mem_To_Reg_MEM;  
            Write_Reg           <= i_Write_Reg_MEM;          
        end    
    end
 
endmodule
