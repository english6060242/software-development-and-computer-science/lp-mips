`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01.06.2022 09:15:07
// Design Name: 
// Module Name: UART_RX
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module UART_RX
#(
    parameter   NB_UART = 8,    // Number of data bits
    parameter   SB_TICK = 16,   // Number of ticks required for stop bits
    parameter   NB_STAT = 2,    // Number of bits to represent states    
    parameter   NB_CONT = 4,
    parameter   NB_BRCV = 3
)
(
    // Inputs
    input   wire                                i_clk,
    input   wire                                i_Reset,        
    input   wire                                i_rx,           // rx
    input   wire                                s_tick,         // Baud Rate Gen signal

    // Outputs
    output  reg                                 rx_done_tick,   // Indicates data has been received
    output  wire    signed  [NB_UART - 1 : 0]   dout
);
 
    // States
    localparam              [NB_STAT - 1 : 0]   idle  = 2'b00;
    localparam              [NB_STAT - 1 : 0]   start = 2'b01;   // s_reg counts up to 7
    localparam              [NB_STAT - 1 : 0]   data  = 2'b10;   // s_reg counts up to 15
    localparam              [NB_STAT - 1 : 0]   stop  = 2'b11;   // s_reg counts up to SB_TICK
    
    // Internal
    reg                     [NB_STAT - 1 : 0]   state_reg;      // Current state and next state
    reg                     [NB_STAT - 1 : 0]   state_next;     // s_reg counts the number of ticks that have occurred
    reg                     [NB_CONT - 1 : 0]   s_reg;          
    reg                     [NB_CONT - 1 : 0]   s_next;
    reg                     [NB_BRCV - 1 : 0]   n_reg;          // n_reg counts the number of received bits
    reg                     [NB_BRCV - 1 : 0]   n_next;
    reg     signed          [NB_UART - 1 : 0]   b_reg;          // b_reg stores received data (parallelized)
    reg     signed          [NB_UART - 1 : 0]   b_next;
    
    // States - Sequential
    always @(posedge i_clk, posedge i_Reset)
    begin
        if (i_Reset)
        begin
            state_reg <= idle;
            s_reg <= 0;
            n_reg <= 0;
            b_reg <= 0;
        end
        else
        begin
            state_reg <= state_next;
            s_reg <= s_next;
            n_reg <= n_next;
            b_reg <= b_next;
        end
    end
    
    // Get next state - Combinational
    always @(*)
    begin
        // Default values: Next state = current state, rx_done_tick = 0
        state_next = state_reg;
        rx_done_tick = 1'b0;
        s_next = s_reg;
        n_next = n_reg;
        b_next = b_reg;
        
        case(state_reg)
            idle:
                if (~i_rx)              // If rx == 0 then
                begin
                    state_next = start; // transmission should start
                    s_next = 0;         // Tick count remains zero
                end
            start:
                if(s_tick)
                begin
                    if(s_reg == 7)      // At the middle of the start bit period
                    begin 
                        state_next = data;
                        s_next = 0;
                        n_next = 0;
                    end
                    else
                    begin
                        s_next = s_reg + 1; // Increment tick counter except when it reaches 7 (half the tick count)
                    end 
                end                              
            data:
            if(s_tick)  // Upon a tick
                if(s_reg == 15)
                    begin
                        s_next = 0; // Reset tick count
                        b_next = {i_rx, b_reg [7 : 1]}; // Concatenate rx with b_reg where the data is assembled with rx bits
                        if(n_reg == (NB_UART - 1)) // If all data bits are received, the next state will be STOP
                            state_next = stop;  // n_reg counter is reset in the START state
                        else
                            n_next = n_reg + 1; // Increment received data counter unless it reaches DBIT - 1
                    end
                else
                    s_next = s_reg + 1;     // Increment tick count unless it reaches 15
        stop:
            if(s_tick)  // Upon a tick
                if(s_reg == (SB_TICK - 1))
                    begin
                        state_next = idle;      // Return to initial state waiting for another transfer.    
                        rx_done_tick = 1'b1;    // If all ticks are in the stop state, the process is complete
                    end      
                else
                    s_next = s_reg + 1; // Increment tick counter until it reaches the maximum value                                                                       
    endcase     
end

// Output
assign dout = b_reg;

endmodule
