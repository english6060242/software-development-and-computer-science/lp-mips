`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01.06.2022 10:50:18
// Design Name: 
// Module Name: EX_MEM
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module EX_MEM
#(
    parameter   NB_DATA  = 32,
    parameter   NB_RDIR  = 5,
    parameter   NB_ALUOP = 4,
    parameter   NB_MUX4  = 2
 )
(
    // clk and reset
    input   wire                         i_clk,                  // Clock
    input   wire                         i_Reset,                // Reset
    
    // Save data from the EX stage to be used in the MEM stage
    input   wire    [NB_DATA  - 1 : 0]   i_ALU_Result_EX,       // ALU result
    input   wire    [NB_DATA  - 1 : 0]   i_Reg_Read_Data_2_EX,  // To write to memory from register file
    input   wire    [NB_DATA  - 1 : 0]   i_PC_Plus_Four_EX,    // PC + 4
    
    input   wire    [NB_RDIR  - 1 : 0]   i_rs_EX,
    input   wire    [NB_RDIR  - 1 : 0]   i_rt_EX,
    input   wire    [NB_RDIR  - 1 : 0]   i_rd_EX,
    input   wire    [NB_RDIR  - 1 : 0]   i_Dest_Register_EX,    // Write register
    
    // Control signals to be passed from EX stage to MEM stage
    input   wire                         i_Mem_Read_EX,             // Read data memory
    input   wire                         i_Mem_Write_EX,            // Write data memory
    input   wire                         i_Signed_EX,            // Write data memory
    input   wire    [NB_MUX4  - 1 : 0]   i_BHW_EX,                     // Read/write Word, Half Word, or Byte
    
    // Control signals to be passed from EX stage to WB stage
    input   wire    [NB_MUX4  - 1 : 0]   i_Mem_To_Reg_EX,           // Select data to be written to register
    input   wire                         i_Write_Reg_EX,            // Signal to write registers 
    
    // Outputs
        // Save data from EX stage to be used in EX stage
    output  wire    [NB_DATA  - 1 : 0]   o_ALU_Result_MEM,       // ALU result
    output  wire    [NB_DATA  - 1 : 0]   o_Reg_Read_Data_2_MEM,
    output  wire    [NB_DATA  - 1 : 0]   o_PC_Plus_Four_MEM,
    
    output  wire    [NB_RDIR  - 1 : 0]   o_rs_MEM,
    output  wire    [NB_RDIR  - 1 : 0]   o_rt_MEM,
    output  wire    [NB_RDIR  - 1 : 0]   o_rd_MEM,
    output  wire    [NB_RDIR  - 1 : 0]   o_Dest_Register_MEM,    // Write register
    
    // Control signals to be passed from EX stage to MEM stage
    output  wire                         o_Mem_Read_MEM,             // Read data memory
    output  wire                         o_Mem_Write_MEM,            // Write data memory
    output  wire    [NB_MUX4  - 1 : 0]   o_BHW_MEM,                  // Read/write Word, Half Word, or Byte
    output  wire                         o_Signed_MEM,               // Signal to write registers 
    
    // Control signals to be passed from EX stage to WB stage
    output  wire    [NB_MUX4  - 1 : 0]   o_Mem_To_Reg_MEM,           // Select data to be written to register
    output  wire                         o_Write_Reg_MEM            // Signal to write registers 
        
 );  
 
    // Save data from the EX stage to be used in the MEM stage
    reg    [NB_DATA  - 1 : 0]   ALU_Result;
    reg    [NB_DATA  - 1 : 0]   Reg_Read_Data_2;
    reg    [NB_DATA  - 1 : 0]   PC_Plus_Four;
    
    
    // Control signals to be passed from the EX stage to the MEM stage
    reg                         Mem_Read;             // Read data memory
    reg                         Mem_Write;            // Write data memory
    reg    [NB_MUX4  - 1 : 0]   BHW;                     // Read/write Word, Half Word, or Byte
    reg                         Signed;             // Write data memory
    
    // Control signals to be passed from the EX stage to the WB stage
    reg    [NB_MUX4  - 1 : 0]   Mem_To_Reg;           // Select data to be written to register
    reg                         Write_Reg;            // Signal to write registers 
    
	// Hazard detection signals
    reg    [NB_RDIR  - 1 : 0]   rs;
    reg    [NB_RDIR  - 1 : 0]   rt;
    reg    [NB_RDIR  - 1 : 0]   rd;
    reg    [NB_RDIR  - 1 : 0]   Dest_Register;
    
    // Save data from the EX stage to be used in the MEM stage
    assign o_ALU_Result_MEM         = ALU_Result;
    assign o_Reg_Read_Data_2_MEM    = Reg_Read_Data_2;
    assign o_PC_Plus_Four_MEM       = PC_Plus_Four;
    
    assign  o_rs_MEM                = rs;
    assign  o_rt_MEM                = rt;
    assign  o_rd_MEM                = rd;
    assign  o_Dest_Register_MEM     = Dest_Register;
    
    // Control signals to be passed from the EX stage to the MEM stage 
    assign o_Mem_Read_MEM           = Mem_Read;             // Read data memory
    assign o_Mem_Write_MEM          = Mem_Write;            // Write data memory
    assign o_BHW_MEM                = BHW;                     // Read/write Word, Half Word, or Byte
    assign o_Signed_MEM             = Signed;  
    
    
    // Control signals to be passed from the EX stage to the WB stage
    assign o_Mem_To_Reg_MEM         = Mem_To_Reg;           // Select data to be written to register
    assign o_Write_Reg_MEM          = Write_Reg;            // Signal to write registers
    

    always @(negedge i_clk)
    begin
        if(i_Reset)
        begin
            ALU_Result          <= 32'b0;
            Reg_Read_Data_2     <= 32'b0;
            PC_Plus_Four        <= 32'b0;
                  
            rs                  <= 5'b0;
            rt                  <= 5'b0;
            rd                  <= 5'b0;
            Dest_Register       <= 5'b0;              
                  
            Mem_Read            <= 1'b0;           
            Mem_Write           <= 1'b0;            
            BHW                 <= 2'b0;
            Signed              <= 1'b0;                     
    
            Mem_To_Reg          <= 2'b0;    
            Write_Reg           <= 1'b0;       
        end
        else 
        begin
            ALU_Result          <= i_ALU_Result_EX;
            Reg_Read_Data_2     <= i_Reg_Read_Data_2_EX;
            PC_Plus_Four        <= i_PC_Plus_Four_EX;
            
            rs                  <= i_rs_EX;
            rt                  <= i_rt_EX;
            rd                  <= i_rd_EX;
            Dest_Register       <= i_Dest_Register_EX;  
                                    
            Mem_Read            <= i_Mem_Read_EX;           
            Mem_Write           <= i_Mem_Write_EX;            
            BHW                 <= i_BHW_EX;    
            Signed              <= i_Signed_EX;                   
    
            Mem_To_Reg          <= i_Mem_To_Reg_EX;  
            Write_Reg           <= i_Write_Reg_EX;          
        end    
    end
    
endmodule
