`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Creation Date: 01.06.2022 09:16:03
// Design Name: 
// Module Name: MIPS
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module MIPS
#(
    parameter   NB_DATA     = 32,
    parameter   NB_UART     = 8,
    parameter   NB_DIR_PC   = 32,
    parameter   NB_ALUOP    = 4,
    parameter   NB_SHAMT    = 5,
    parameter   NB_OPCODE   = 6,
    parameter   NB_FUNC     = 6,
    parameter   NB_RDIR     = 5,     // Bits for register addressing
    parameter   NB_MUX4     = 2,
    parameter   NB_SEND     = 384, // Number of bits to send (table)
    parameter   NB_ALLR     = 1024,  // Number of bits for registers to send
    parameter   NB_DMEM     = 320    // Number of bits for data memory to send
 )
(
    // Inputs
    input   wire                          i_clk,                  // Clock
    input   wire                          i_Reset,                // Reset  
    input   wire                          i_Stall,
    
    input   wire    [NB_DATA  - 1 : 0]    i_Prog_Write_Address,
    input   wire    [NB_UART  - 1 : 0]    i_Prog_MEM_Data,
    input   wire                          i_Prog_MEM_Write,
    
    // Outputs
    output  wire                          o_HLT,
    output  wire    [NB_SEND  - 1 : 0]    o_table
 );

// Internal
wire                            Halt;
wire                            Stall;
wire                            hd_Stall;

wire    [NB_DIR_PC - 1 : 0] PC_In;  // Next program memory address
wire    [NB_DIR_PC - 1 : 0] PC_Out;    // PC output indicates the address to read from program memory

// Program Memory
wire    [NB_DATA - 1 : 0]   Instruction_IF;
wire    [NB_DATA - 1 : 0]   Instruction_ID;

// Adder
wire    [NB_DATA - 1 : 0]   PC_Plus_Four_IF;
wire    [NB_DATA - 1 : 0]   PC_Plus_Four_ID;
wire    [NB_DATA - 1 : 0]   PC_Plus_Four_EX;
wire    [NB_DATA - 1 : 0]   PC_Plus_Four_MEM;
wire    [NB_DATA - 1 : 0]   PC_Plus_Four_WB;

// MUX 1
wire    [NB_DATA - 1 : 0]   Next_Or_Branch;  // Instruction [15 - 0] << 2

// MUX 2
wire    [NB_DATA - 1 : 0]   NextOrBranch_Or_Jump;  // Instruction [15 - 0] << 2	

// Decoder
wire                                Jump_ID;
wire                                Jump_Register_ID;
wire                                Branch_ID;
wire                                On_Equal_ID;
wire                                PC_Write_ID;
wire                                Mem_Read_ID;
wire                                Mem_Write_ID;
wire                                Signed_ID;
wire    [NB_MUX4    - 1 : 0]        BHW_ID;
wire                                Write_Reg_ID;
wire    [NB_MUX4    - 1 : 0]        Mem_To_Reg_ID;
wire    [NB_MUX4    - 1 : 0]        Reg_Dest_ID;
wire                                ALU_Src_ID;
wire                                Halt_ID;

wire                                Jalr_ID;

wire    [NB_FUNC  - 1 : 0]          Function_EX; 
wire                                Jump_Register_EX;

wire                                Mem_Read_EX;
wire                                Mem_Write_EX;
wire                                Signed_EX;
wire    [NB_MUX4    - 1 : 0]        BHW_EX;
wire                                Write_Reg_EX;
wire    [NB_MUX4    - 1 : 0]        Mem_To_Reg_EX;
wire    [NB_MUX4    - 1 : 0]        Reg_Dest_EX;
wire                                ALU_Src_EX;

wire    [NB_ALUOP   - 1 : 0]        ALU_Operation_ID;
wire    [NB_ALUOP   - 1 : 0]        ALU_Operation_EX;

wire                                Mem_Read_MEM;
wire                                Mem_Write_MEM;
wire                                Signed_MEM;
wire    [NB_MUX4    - 1 : 0]        BHW_MEM;
wire                                Write_Reg_MEM;
wire    [NB_MUX4    - 1 : 0]        Mem_To_Reg_MEM;

wire                                Write_Reg_WB;
wire    [NB_MUX4    - 1 : 0]        Mem_To_Reg_WB;



wire    [NB_DATA    - 1 : 0]        Reg_Read_Data_1_ID;
wire    [NB_DATA    - 1 : 0]        Reg_Read_Data_2_ID;

wire    [NB_DATA    - 1 : 0]        Reg_Read_Data_1_EX;
wire    [NB_DATA    - 1 : 0]        Reg_Read_Data_2_EX;

wire    [NB_DATA    - 1 : 0]        Reg_Read_Data_2_MEM;

wire    [NB_DATA    - 1 : 0]        Extended_Offset_Immediate_ID;
wire    [NB_DATA    - 1 : 0]        Shifted_Offset_Immediate_ID;

wire    [NB_DATA    - 1 : 0]        PC_Branch_Dir_ID;
wire    [NB_DATA    - 1 : 0]        PC_Jump_ID;
wire    [28         - 1 : 0]        Shifted_Address_ID;

    // For Jumps
wire    [NB_DATA    - 1 : 0]        PC_Jump_EX;
wire    [NB_DATA    - 1 : 0]        PC_Branch_Dir_EX;
wire                                PC_Src_EX;
wire                                IF_Flush_EXJump_EX;

wire    [NB_DATA    - 1 : 0]        Foward_Or_Jump_Register;

wire    [NB_DATA    - 1 : 0]        ALU_Result_EX;
wire    [NB_DATA    - 1 : 0]        ALU_Result_MEM;
wire    [NB_DATA    - 1 : 0]        ALU_Result_WB;

wire    [NB_DATA    - 1 : 0]        Mem_Data_Out_MEM;
wire    [NB_DATA    - 1 : 0]        Mem_Data_Out_WB;

wire    [NB_DATA    - 1 : 0]        C1;
wire    [NB_DATA    - 1 : 0]        C2;

wire    [NB_MUX4    - 1 : 0]        F1;
wire    [NB_MUX4    - 1 : 0]        F2;
wire    [NB_MUX4    - 1 : 0]        F3;
wire    [NB_MUX4    - 1 : 0]        F4;
wire    [NB_MUX4    - 1 : 0]        F5;
wire    [NB_MUX4    - 1 : 0]        F6;

wire    [NB_DATA    - 1 : 0]        Foward_Reg_Read_Data_2_EX;
wire    [NB_DATA    - 1 : 0]        Foward_Reg_Read_Data_2_MEM;

wire    [NB_DATA    - 1 : 0]        ALU_Data_In_1;
wire    [NB_DATA    - 1 : 0]        ALU_Data_In_2;

wire                                Equal_ID;

wire    [NB_DATA    - 1 : 0]        Extended_Offset_Immediate_EX;

wire    [NB_DATA    - 1 : 0]        Reg_Or_Immediate_EX;

wire    [NB_RDIR    - 1 : 0]        rs_EX;
wire    [NB_RDIR    - 1 : 0]        rt_EX;
wire    [NB_RDIR    - 1 : 0]        rd_EX;
wire    [NB_RDIR    - 1 : 0]        Dest_Register_EX;

wire    [NB_RDIR    - 1 : 0]        rs_MEM;
wire    [NB_RDIR    - 1 : 0]        rt_MEM;
wire    [NB_RDIR    - 1 : 0]        rd_MEM;
wire    [NB_RDIR    - 1 : 0]        Dest_Register_MEM;

wire    [NB_RDIR    - 1 : 0]        rs_WB;
wire    [NB_RDIR    - 1 : 0]        rt_WB;
wire    [NB_RDIR    - 1 : 0]        rd_WB;
wire    [NB_RDIR    - 1 : 0]        Dest_Register_WB;

wire    [NB_DATA    - 1 : 0]        Reg_Data_In_WB;

// For the table
wire    [NB_ALLR - 1 : 0]   All_Registers;
wire    [NB_DMEM - 1 : 0]   Data_MEM_Block;

reg     [NB_DATA - 1 : 0]   CONT;

assign  o_HLT = Halt_ID;

assign o_table[31 : 0] = PC_Out;
assign o_table[63 : 32] = CONT;
assign o_table[223 : 64] = All_Registers;
assign o_table[383 : 224] = Data_MEM_Block;

assign Stall = hd_Stall | i_Stall;

always @(negedge i_clk)
begin
    if(i_Reset)
    begin
        CONT <= 32'b0;
    end
    else if (i_Stall || Halt_ID)
    begin
        CONT <= CONT;
    end
    else
    begin
        CONT <= CONT + 1;
    end
end


PC
#(
    .NB_DIR_PC              (NB_DIR_PC)
 )
u_PC_1
(
    .i_clk                  (i_clk),
    .i_Reset                (i_Reset),
    .i_Stall                (Stall),
    .i_Halt                 (Halt_ID),     
    .i_PC_In                (PC_In),  // Next program memory address
    .o_PC_Out               (PC_Out)   // PC output indicates the address to read from program memory
);

Program_Memory
#(
    .NB_DATA                (NB_DATA),
    .NB_UART                (NB_UART)
 )
u_Program_Memory_1
(
    // Inputs
    .i_clk                  (i_clk),             
    .i_Reset                (i_Reset),               
    //.i_Prog_Mem_Read        (), //read enable
    .i_Prog_Mem_Write       (i_Prog_MEM_Write), 
    .i_Address              (PC_Out),
    .i_Write_Address        (i_Prog_Write_Address),
    .i_Write_Data           (i_Prog_MEM_Data),
    
    // Outputs
    .o_Instruction          (Instruction_IF)
 );
 
 Adder
#(
    .NB_DATA                (NB_DATA)
 )
 u_Adder_1
 (
    // Inputs
    .i_Adder_In             (PC_Out),
    // Outputs
    .o_Adder_Out            (PC_Plus_Four_IF)
 );
 
IF_ID
#(
    .NB_DATA                (NB_DATA)
 )
u_IF_ID_1 
(
    // Inputs
    .i_clk                     (i_clk),                  
    .i_Reset                   (i_Reset),              
    .i_Stall                   (Stall),   
    .i_Halt                    (Halt_ID),    
    .i_Instruction_IF          (Instruction_IF),
    .i_PC_Plus_Four_IF         (PC_Plus_Four_IF),


    // Outputs
    .o_Instruction_ID           (Instruction_ID),
    .o_PC_Plus_Four_ID          (PC_Plus_Four_ID)
 ); 
 
 MUX
#(
    .NB_DATA                (NB_DATA)
 )
 u_MUX_1_Next_Or_Branch
 (
    // Inputs
    .i_MUX_0                (PC_Plus_Four_IF),
    .i_MUX_1                (PC_Branch_Dir_ID),
    .i_MUX_Select           ((Branch_ID & Equal_ID & On_Equal_ID) | (((~Equal_ID) &(~On_Equal_ID)) & Branch_ID)),

    // Outputs
    .o_MUX                  (Next_Or_Branch)
 );
 
  MUX
#(
    .NB_DATA                (NB_DATA)
 )
 u_MUX_Or_Jump_2
 (
    // Inputs
    .i_MUX_0                (Next_Or_Branch),
    .i_MUX_1                ({(PC_Plus_Four_ID[31:28]),Shifted_Address_ID}),
    .i_MUX_Select           (Jump_ID),
    // Outputs
    .o_MUX                  (NextOrBranch_Or_Jump)
 );

MUX4
#(
    .NB_DATA                (NB_DATA)    // Bits for register addressing to write
 )
u_MUX_Jump_Reg
 (
    // Inputs
    .i_MUX_0                (Reg_Read_Data_1_ID),
    .i_MUX_1                (Reg_Data_In_WB),
    .i_MUX_2                (ALU_Result_MEM),
    .i_MUX_3                (32'b0),
    .i_MUX_Select           (F6),
    // Outputs
    .o_MUX                  (Foward_Or_Jump_Register)
 );

  MUX
#(
    .NB_DATA                (NB_DATA)
 )
 u_MUX_Or_Jump_Register_3
 (
    // Inputs
    .i_MUX_0                (NextOrBranch_Or_Jump),
    .i_MUX_1                (Foward_Or_Jump_Register),
    .i_MUX_Select           (Jump_Register_ID),
    // Outputs
    .o_MUX                  (PC_In)
 );
 
Decoder
#(
    .OP_CODE(6),
    .NB_FUNC(6),
    .ALU_OPB(4),
    .NB_2BIT(2),
    .ALU_DEC(3)
 )
 u_Decoder_1
 (
    // Inputs
    .i_Opcode               (Instruction_ID[31:26]),
    .i_Function             (Instruction_ID[5:0]),

    // Outputs
    .o_Jump                 (Jump_ID),
    .o_Jump_Register        (Jump_Register_ID),
    .o_Branch               (Branch_ID),
    .o_On_Equal             (On_Equal_ID),
    .o_Mem_Read             (Mem_Read_ID),
    .o_Mem_Write            (Mem_Write_ID),
    .o_BHW                  (BHW_ID),
    .o_Signed_Unsigned      (Signed_Unsigned_ID),
    .o_Mem_To_Reg           (Mem_To_Reg_ID),
    .o_Write_Reg            (Write_Reg_ID),
    .o_Reg_Dest             (Reg_Dest_ID),
    .o_ALU_Op               (ALU_Operation_ID),
    .o_ALU_Src              (ALU_Src_ID),
    .o_Halt                 (Halt_ID)
 );
 
RegisterBlock
#(
    .NB_DATA                (NB_DATA),
    .NB_RDIR                (NB_RDIR),
    .NB_ALLR                (NB_ALLR)
  )
u_RegisterBlock_1
(
    .i_clk	                (i_clk),
    .i_Reset                (i_Reset),
    .i_Src_Register_Out_1   (Instruction_ID[25:21]),   // Addressing for output 1
    .i_Src_Register_Out_2   (Instruction_ID[20:16]),   // Addressing for output 2
    .i_Dest_Register        (Dest_Register_WB),        // Addressing for write
    .i_Data_In              (Reg_Data_In_WB),              // Data for write
    .i_Write_Reg            (Write_Reg_WB),            // Write signal
    .o_Read_Data_1          (Reg_Read_Data_1_ID),
    .o_Read_Data_2          (Reg_Read_Data_2_ID),
    .o_All_Registers        (All_Registers)
 );
 
Sign_Extend
#(
    .NB_DATA_IN             (16),
    .NB_DATA_OUT            (32)
 )
 u_Sign_Extend_1
 (
    // Inputs
    .i_SE_In                (Instruction_ID[15:0]),
    // Outputs
    .o_SE_Out               (Extended_Offset_Immediate_ID)
 );
 
Shift_Left_2_32
#(
    .NB_DATA                (32)
)
u_Shift_Left_2_32_1
(
    // Inputs
    .i_SL232_IN             (Extended_Offset_Immediate_ID),
    // Outputs
    .o_SL232_OUT            (Shifted_Offset_Immediate_ID)
 );

Adder_2
#(
    .NB_DATA                (NB_DATA)
 )
 u_Adder_2_1
 (
    // Inputs
    .i_Data_1               (PC_Plus_Four_ID),
    .i_Data_2               (Shifted_Offset_Immediate_ID),
    // Outputs
    .o_AD2_Result           (PC_Branch_Dir_ID)
 );

Shift_Left_2_28
#(
    .NB_DATA_IN             (26),
    .NB_DATA_OUT            (28)
 )
 Shift_Left_2_28_1
 (
    // Inputs
    .i_SL228_IN             (Instruction_ID[25:0]),
    // Outputs
    .o_SL228_OUT            (Shifted_Address_ID)
 );

 MUX4
#(
    .NB_DATA                (NB_DATA)    // Bits for register addressing to write
 )
u_MUX_4_Foward_Comp_4
 (
    // Inputs
    .i_MUX_0                (Reg_Read_Data_1_ID),
    .i_MUX_1                (Reg_Data_In_WB),
    .i_MUX_2                (ALU_Result_MEM),
    .i_MUX_3                (32'b0),
    .i_MUX_Select           (F3),
    
    // Outputs
    .o_MUX                  (C1)
 );
 

MUX4
#(
    .NB_DATA                (NB_DATA)    // Bits for register addressing to write
 )
u_MUX_4_Foward_Comp_5
 (
    // Inputs
    .i_MUX_0                (Reg_Read_Data_2_ID),
    .i_MUX_1                (Reg_Data_In_WB),
    .i_MUX_2                (ALU_Result_MEM),
    .i_MUX_3                (32'b0),
    .i_MUX_Select           (F4),
    
    // Outputs
    .o_MUX                  (C2)
 );

Eq_Comparator
#(
    .NB_DATA                (NB_DATA)
 )
u_Eq_Comparator_1
 (
    // Inputs
    .i_Data_1               (C1),
    .i_Data_2               (C2),
    
    // Outputs
    .o_Equal                (Equal_ID)
 );
 
Hazard_Detection_Unit
#(
    .NB_RDIR                (NB_RDIR)   
 )
u_Hazard_Detection_Unit_1
 (
    // Inputs
    .i_Mem_Read_EX          (Mem_Read_EX),
    .i_rs_ID                (Instruction_ID[25:21]),
    .i_rt_ID                (Instruction_ID[20:16]),
    .i_rt_EX                (rt_EX),
    //.i_Dest_Register_EX     (Dest_Register_EX),
    
    .o_Stall                (hd_Stall)
 );

ID_EX
#(
    .NB_DATA                (NB_DATA),
    .NB_FUNC                (NB_FUNC),
    .NB_RDIR                (NB_RDIR),
    .NB_ALUOP               (NB_ALUOP),
    .NB_MUX4                (NB_MUX4)
 )
u_ID_EX_1
(
    // clk and reset
    .i_clk                  (i_clk),                  
    .i_Reset                (i_Reset),               
    
    // Save data from the ID stage to be used in the EX stage
    .i_Reg_Read_Data_1_ID   (Reg_Read_Data_1_ID),
    .i_Reg_Read_Data_2_ID   (Reg_Read_Data_2_ID),
    .i_Immediate_ID         (Extended_Offset_Immediate_ID),
    .i_PC_Plus_Four_ID      (PC_Plus_Four_ID),
    
    // For jumps
    .i_PC_Jump_ID           ({(PC_Plus_Four_ID[31:28]),Shifted_Address_ID}),
    .i_PC_Branch_Dir_ID     (PC_Branch_Dir_ID),
    .i_PC_Src_ID            ((Branch_ID & Equal_ID & On_Equal_ID) | ((~(Equal_ID & On_Equal_ID)) & Branch_ID)), 
    .i_Jump_ID              (Jump_ID),
    .i_Jump_Register_ID     (Jump_Register_ID),
    //.i_IF_Flush_ID          (((Branch_ID & Equal_ID & On_Equal_ID) | ((~(Equal_ID & On_Equal_ID)) & Branch_ID)) | Jump_ID),
    
    // Control signals to be passed from ID stage to EX stage
    .i_Reg_Dest_ID          (Reg_Dest_ID),
    .i_ALU_Src_ID           (ALU_Src_ID),
    .i_Function_ID          (Instruction_ID[5:0]),
    .i_ALU_Operation_ID     (ALU_Operation_ID), 
    
    // Control signals to be passed from ID stage to MEM stage
    .i_Write_Reg_ID         (Write_Reg_ID),            // Signal to write registers 
    .i_Branch_ID            (Branch_ID),               // Signal to write registers 
    .i_Mem_Read_ID          (Mem_Read_ID),             // Read data memory
    .i_Mem_Write_ID         (Mem_Write_ID),            // Write data memory
    .i_BHW_ID               (BHW_ID),                     // Read/write Word, Half Word or Byte
    .i_Signed_ID            (Signed_Unsigned_ID),
    
    // Control signals to be passed from ID stage to WB stage
    .i_Mem_To_Reg_ID        (Mem_To_Reg_ID),           // Selects data to be written to register
    
	// Signals for hazard detection 
    .i_rs_ID                (Instruction_ID[25:21]),
    .i_rt_ID                (Instruction_ID[20:16]),
    .i_rd_ID                (Instruction_ID[15:11]),
    
    .i_Stall                (Stall),                // Stop pipeline for control hazards	 

    // Outputs
        // Save data from the ID stage to be used in the EX stage
    .o_Reg_Read_Data_1_EX   (Reg_Read_Data_1_EX),
    .o_Reg_Read_Data_2_EX   (Reg_Read_Data_2_EX),
    .o_Immediate_EX         (Extended_Offset_Immediate_EX),
    .o_PC_Plus_Four_EX      (PC_Plus_Four_EX),
    
    // For jumps
    .o_PC_Jump_EX           (PC_Jump_EX),
    .o_PC_Branch_Dir_EX     (PC_Branch_Dir_EX),
    .o_PC_Src_EX            (PC_Src_EX), 
    .o_Jump_EX              (Jump_EX),
    .o_Jump_Register_EX     (Jump_Register_EX),
    
    // Control signals to be passed from ID stage to EX stage
    .o_Reg_Dest_EX          (Reg_Dest_EX),
    .o_ALU_Src_EX           (ALU_Src_EX),
    .o_Function_EX          (Function_EX),
    .o_ALU_Operation_EX     (ALU_Operation_EX), 
    
    // Control signals to be passed from ID stage to MEM stage
    .o_Write_Reg_EX         (Write_Reg_EX),            // Signal to write registers 
    .o_Branch_EX            (Branch_EX),               // Signal to write registers 
    .o_Mem_Read_EX          (Mem_Read_EX),             // Read data memory
    .o_Mem_Write_EX         (Mem_Write_EX),            // Write data memory
    .o_BHW_EX               (BHW_EX),                     // Read/write Word, Half Word or Byte
    .o_Signed_EX            (Signed_EX),
    
    // Control signals to be passed from ID stage to WB stage
    .o_Mem_To_Reg_EX        (Mem_To_Reg_EX),           // Selects data to be written to register
    
	// Signals for hazard detection 
    .o_rs_EX                (rs_EX),
    .o_rt_EX                (rt_EX),
    .o_rd_EX                (rd_EX)
 
 );
 
MUX4
#(
    .NB_DATA                (NB_RDIR)    // Bits for register addressing to write
 )
u_MUX_Dest_Register_6
 (
    // Inputs
    .i_MUX_0                (rt_EX),
    .i_MUX_1                (rd_EX),
    .i_MUX_2                (31),
    .i_MUX_3                (0),
    .i_MUX_Select           (Reg_Dest_EX),
    // Outputs
    .o_MUX                  (Dest_Register_EX)
 );

MUX4
#(
    .NB_DATA                (NB_DATA)    // Bits for register addressing to write
 )
u_MUX_7_Data_Fowarding_1
 (
    // Inputs
    .i_MUX_0                (Reg_Read_Data_1_EX),
    .i_MUX_1                (Reg_Data_In_WB),
    .i_MUX_2                (ALU_Result_MEM),
    .i_MUX_3                (32'b0),
    .i_MUX_Select           (F1),
    // Outputs
    .o_MUX                  (ALU_Data_In_1)
 );
 
MUX4
#(
    .NB_DATA                (NB_DATA)    // Bits for register addressing to write
 )
u_MUX_8_Data_Fowarding_2
 (
    // Inputs
    //.i_MUX_0                (Reg_Or_Immediate_EX),
    .i_MUX_0                (Reg_Read_Data_2_EX),
    .i_MUX_1                (Reg_Data_In_WB),
    .i_MUX_2                (ALU_Result_MEM),
    .i_MUX_3                (32'b0),
    .i_MUX_Select           (F2),
    // Outputs
    //.o_MUX                  (ALU_Data_In_2)
    .o_MUX                  (Reg_Or_Immediate_EX)
 );
 
  MUX
#(
    .NB_DATA                (NB_DATA)
 )
 u_MUX_9_ALU_Data_2_Select
 (
    // Inputs
    .i_MUX_0                (Reg_Or_Immediate_EX),
    .i_MUX_1                (Extended_Offset_Immediate_EX),
    .i_MUX_Select           (ALU_Src_EX),
    // Outputs
    .o_MUX                  (ALU_Data_In_2)
 );

ALU
#(
    .NB_DATA                (NB_DATA),
    .NB_ALUOP               (NB_ALUOP),
    .NB_SHAMT               (NB_SHAMT)
  )
u_ALU_1
(
    // Inputs
    .i_Data_1               (ALU_Data_In_1),
    .i_Data_2               (ALU_Data_In_2),
    .i_Shamt                (Extended_Offset_Immediate_EX[10:6]),
    .i_Op                   (ALU_Operation_EX),
    
    // Outputs
    .o_Zero                 (ALU_Zero_EX),
    .o_ALU_Result           (ALU_Result_EX)
 );
 
Fowarding_Unit
#(
    .NB_RDIR                (NB_RDIR),
    .NB_MUX4                (NB_MUX4)
 )
u_Fowarding_Unit_1
 (
    // Inputs
    //.i_Write_Reg_EX         (Write_Reg_EX),
    .i_Write_Reg_MEM        (Write_Reg_MEM),
    .i_Write_Reg_WB         (Write_Reg_WB),
    .i_Mem_Write_EX         (Mem_Write_EX),
    //.i_Mem_Write_MEM        (Mem_Write_MEM),
    .i_Branch_ID            (Branch_ID),
    .i_Jump_Register_ID     (Jump_Register_ID),
    .i_rs_ID                (Instruction_ID[25:21]),
    .i_rs_EX                (rs_EX),
    //.i_rs_MEM               (rs_MEM),
    .i_rt_ID                (Instruction_ID[20:16]),
    .i_rt_EX                (rt_EX),
    //.i_rt_MEM               (rt_MEM),
    //.i_rd_MEM               (rd_MEM),
    //.i_rd_WB                (rd_WB),
    .i_Dest_Register_MEM    (Dest_Register_MEM),
    .i_Dest_Register_WB     (Dest_Register_WB),
    
    // Outputs
    .o_F1                   (F1),
    .o_F2                   (F2),
    .o_F3                   (F3),
    .o_F4                   (F4),
    .o_F5                   (F5),
    .o_F6                   (F6)
 );

 
MUX4
#(
    .NB_DATA                (NB_DATA)    // Bits for register addressing to write
 )
u_MUX_14_MEM_Data_Fowarding_2
 (
    // Inputs
    .i_MUX_0                (Reg_Read_Data_2_EX),
    .i_MUX_1                (Reg_Data_In_WB),
    .i_MUX_2                (ALU_Result_MEM),
    .i_MUX_3                (32'b0),
    .i_MUX_Select           (F5),
    // Outputs
    .o_MUX                  (Foward_Reg_Read_Data_2_EX)
 );

EX_MEM
#(
    .NB_DATA                (NB_DATA),
    .NB_RDIR                (NB_RDIR),
    .NB_ALUOP               (NB_ALUOP),
    .NB_MUX4                (NB_MUX4)
 )
u_EX_MEM_1
(
    .i_clk                  (i_clk),                  
    .i_Reset                (i_Reset),               

    .i_ALU_Result_EX        (ALU_Result_EX),        
    .i_Reg_Read_Data_2_EX   (Foward_Reg_Read_Data_2_EX),
    .i_PC_Plus_Four_EX      (PC_Plus_Four_EX),    
    
    .i_rs_EX                (rs_EX),
    .i_rt_EX                (rt_EX),
    .i_rd_EX                (rd_EX),
    .i_Dest_Register_EX     (Dest_Register_EX),    
    
    .i_Mem_Read_EX          (Mem_Read_EX),             
    .i_Mem_Write_EX         (Mem_Write_EX),         
    .i_Signed_EX            (Signed_EX),          
    .i_BHW_EX               (BHW_EX),                  
    
    .i_Mem_To_Reg_EX        (Mem_To_Reg_EX),          
    .i_Write_Reg_EX         (Write_Reg_EX),            
    
    .o_ALU_Result_MEM       (ALU_Result_MEM),      
    .o_Reg_Read_Data_2_MEM  (Reg_Read_Data_2_MEM),
    .o_PC_Plus_Four_MEM     (PC_Plus_Four_MEM),
    
    .o_rs_MEM               (rs_MEM),
    .o_rt_MEM               (rt_MEM),
    .o_rd_MEM               (rd_MEM),
    .o_Dest_Register_MEM    (Dest_Register_MEM),    
    
    .o_Mem_Read_MEM         (Mem_Read_MEM),            
    .o_Mem_Write_MEM        (Mem_Write_MEM),         
    .o_BHW_MEM              (BHW_MEM),                 
    .o_Signed_MEM           (Signed_MEM),             
    
    .o_Mem_To_Reg_MEM       (Mem_To_Reg_MEM),          
    .o_Write_Reg_MEM        (Write_Reg_MEM)            
 );


Data_Memory
#(
    .NB_DATA                (NB_DATA),
    .CANT_DIR_MEM           (1024),
    .NB_DMEM                (NB_DMEM)
)
u_Data_Memory_1
(
    // Inputs
    .i_clk                  (i_clk),                // Clock
    .i_Reset                (i_Reset),                // Reset
    .i_Data_Mem_Read        (Mem_Read_MEM),
    .i_Data_Mem_Write       (Mem_Write_MEM),
    .i_Extend_Sign          (Signed_MEM), 
    .i_BHW                  (BHW_MEM),
    .i_Address              (ALU_Result_MEM),
    .i_Write_Data           (Reg_Read_Data_2_MEM),
    
    // Outputs
    .o_Read_Data            (Mem_Data_Out_MEM),
    .o_Data_MEM_Block       (Data_MEM_Block)
);

MEM_WB
#(
    .NB_DATA                (NB_DATA),
    .NB_RDIR                (NB_RDIR),
    .NB_ALUOP               (NB_ALUOP),
    .NB_MUX4                (NB_MUX4)
 )
u_MEM_WB_1
(
    .i_clk                  (i_clk),                  
    .i_Reset                (i_Reset),               

    .i_ALU_Result_MEM       (ALU_Result_MEM),       
    .i_Mem_Data_Out_MEM     (Mem_Data_Out_MEM),  
    .i_PC_Plus_Four_MEM     (PC_Plus_Four_MEM),    
    
    .i_rs_MEM               (rs_MEM),
    .i_rt_MEM               (rt_MEM),
    .i_rd_MEM               (rd_MEM),
    .i_Dest_Register_MEM    (Dest_Register_MEM),    
    
    .i_Mem_To_Reg_MEM       (Mem_To_Reg_MEM),          
    .i_Write_Reg_MEM        (Write_Reg_MEM),            
    
    .o_ALU_Result_WB        (ALU_Result_WB),       
    .o_Mem_Data_Out_WB      (Mem_Data_Out_WB),
    .o_PC_Plus_Four_WB      (PC_Plus_Four_WB),
    
    .o_rs_WB                (rs_WB),
    .o_rt_WB                (rt_WB),
    .o_rd_WB                (rd_WB),
    .o_Dest_Register_WB     (Dest_Register_WB),    
     
    .o_Mem_To_Reg_WB       (Mem_To_Reg_WB),          
    .o_Write_Reg_WB        (Write_Reg_WB)            
 );

MUX4
#(
    .NB_DATA                (NB_DATA)    // Bits for register addressing to write
 )
u_MUX_10_Select_WB_Data
 (
    // Inputs
    .i_MUX_0                (ALU_Result_WB),
    .i_MUX_1                (Mem_Data_Out_WB),
    .i_MUX_2                (PC_Plus_Four_WB + 4),
    .i_MUX_3                (0),
    .i_MUX_Select           (Mem_To_Reg_WB),
    // Outputs
    .o_MUX                  (Reg_Data_In_WB)
 );

endmodule
