`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01.06.2022 09:16:30
// Design Name: 
// Module Name: Debug_Unit
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Debug_Unit
#( 
    parameter   NB_DATA = 32,
    parameter   NB_UART = 8,    // Number of data bits
    parameter   W = 8,          // Buffer width
    parameter   OP_BITS = 6,    // Operation bits
    parameter   NB_STAT = 4,    // Number of bits to represent states
    parameter   NB_SEND = 384, // Number of bits to send (table)
    parameter   TC_WORD = 12,   // Number of 32-bit words in the table
    parameter   SB_TICK = 16, 
    parameter   NB_CONT = 4,
    parameter   NB_BRCV = 3,
    parameter   M = 14        // Parameter for Baud Rate Generator
)
(
    input   wire                                    i_clk,                  // Clock
    input   wire                                    i_Reset,                // Reset,         
    
    // Rx Inputs
    input   wire                                    i_rx,

    // MIPS Inputs
    input   wire                                    i_hlt,
    input   wire              [NB_SEND  - 1 : 0]    i_table,   
    
    // Program Memory Outputs
    output  wire              [NB_DATA  - 1 : 0]    o_Prog_Mem_Address,
    output  wire              [NB_UART  - 1 : 0]    o_Prog_Mem_Byte,
    output  wire                                    o_Prog_Mem_Write,
    
    // MIPS Outputs
    output  wire                                    o_Debug_Stall,
    
    // Tx Outputs
    output  wire                                    o_tx   
    
);

    localparam      [NB_STAT  - 1 : 0]   CLP1 = 4'b0000;      // Receive LSB data from CLP  (Number of Program Lines)
    localparam      [NB_STAT  - 1 : 0]   CLP2 = 4'b0001;      
    localparam      [NB_STAT  - 1 : 0]   CLP3 = 4'b0010;
    localparam      [NB_STAT  - 1 : 0]   CLP4 = 4'b0011;      // Receive MSB data from CLP  (Number of Program Lines)
    localparam      [NB_STAT  - 1 : 0]   MEMA = 4'b0100;      // Receive LSB data from a program line
    localparam      [NB_STAT  - 1 : 0]   MEMB = 4'b0101;
    localparam      [NB_STAT  - 1 : 0]   MEMC = 4'b0110;
    localparam      [NB_STAT  - 1 : 0]   MEMD = 4'b0111;      // Receive MSB data from a program line
    localparam      [NB_STAT  - 1 : 0]   EMOD = 4'b1000;      // Receive Operation Mode data (Continuous or Step)
    localparam      [NB_STAT  - 1 : 0]   EXEC = 4'b1001;      // Send OK signal to MIPS for execution during 1 cycle
    localparam      [NB_STAT  - 1 : 0]   REGA = 4'b1010;      // Send LSB data from a register table line (PC + 32 Reg + 10 Datamem[])
    localparam      [NB_STAT  - 1 : 0]   REGB = 4'b1011;
    localparam      [NB_STAT  - 1 : 0]   REGC = 4'b1100;
    localparam      [NB_STAT  - 1 : 0]   REGD = 4'b1101;      // Send MSB data from a register table line
    localparam      [NB_STAT  - 1 : 0]   EHLT = 4'b1110;      // Final state: Do nothing
    
    // UART TX and RX
    wire            [NB_UART  - 1 : 0]  MIPS_rx_data;
    wire                                rx_done_tick;
    wire                                tx_done_tick;
    reg             [NB_UART  - 1 : 0]  Data_Out, Data_Out_Next;        // Register to send bytes from the table   
    reg                                 tx_start_reg, tx_start_next;    // Registers to start transmission
    
    // State
    reg             [NB_STAT  - 1 : 0]  state_reg, state_next;          // State register and next state
    
    // CLP = Number of Program Lines (divided into 4 Bytes)
    wire            [NB_DATA  - 1 : 0]  CLP;                            // Number of program lines
    reg             [NB_UART  - 1 : 0]  CLP_A_In, CLP_A_In_Next;        // Registers to receive bytes from CLP
    reg             [NB_UART  - 1 : 0]  CLP_B_In, CLP_B_In_Next;
    reg             [NB_UART  - 1 : 0]  CLP_C_In, CLP_C_In_Next;
    reg             [NB_UART  - 1 : 0]  CLP_D_In, CLP_D_In_Next;
    
    assign  CLP = {CLP_D_In,CLP_C_In,CLP_B_In,CLP_A_In};                // CLP is formed by the concatenation of the 4 received CLPi bytes
    
    // Counters
    reg             [NB_DATA  - 1 : 0]  CONT, CONT_Next;                // Counter of Bytes written in memory
    reg             [NB_DATA  - 1 : 0]  CONT2, CONT2_Next;              // Counter of Bytes sent
    
    // For writing in program memory
    reg             [NB_UART  - 1 : 0]  Byte_MEM_In, Byte_MEM_In_Next;  // Register to receive instruction bytes 
    reg             [NB_DATA  - 1 : 0]  Prog_Mem_Address, Prog_Mem_Address_Next;
    reg                                 Prog_Mem_Write, Prog_Mem_Write_Next;
    
    // Operation mode (continuous "0" or step "!=0")
    reg             [NB_UART  - 1 : 0]  Mode_Op, Mode_Op_Next;
    
    // Table with the 44 registers to be sent (32 registers from the register bank, PC, number of clocks from start, 10 data mem positions)
    wire             [NB_DATA  - 1 : 0]  table2      [0 : TC_WORD  - 1];
    
    // Stop the processor
    reg                                 Debug_Stall, Debug_Stall_Next;
    
    genvar i;
    
    assign  o_Prog_Mem_Byte = Byte_MEM_In;
    assign  o_Prog_Mem_Address = Prog_Mem_Address;
    assign  o_Prog_Mem_Write = Prog_Mem_Write;
    assign  o_tx_start = tx_start_reg;
    assign  o_Debug_Stall = Debug_Stall;
    assign  dout = Data_Out;
    
    generate
        for(i = 0; i < TC_WORD; i = i + 1)
        begin
            assign table2 [i] = i_table[(NB_DATA  - 1 + NB_DATA * i) : (0 + NB_DATA * i)];
        end
    endgenerate
    
    always @(posedge i_clk, posedge i_Reset)
    begin
        if(i_Reset)   // Upon reset (or initially) the current state is idle.
        begin
            state_reg           <= CLP1;
            Byte_MEM_In         <= 0;
            CLP_A_In            <= 0;
            CLP_B_In            <= 0;
            CLP_C_In            <= 0;
            CLP_D_In            <= 0;
            CONT                <= 0;
            Byte_MEM_In         <= 0;
            Prog_Mem_Address    <= 0;
            Prog_Mem_Write      <= 0;
            Debug_Stall         <= 1;
            Mode_Op             <= 0;
            Data_Out            <= 0;
            CONT2               <= 0;
            tx_start_reg        <= 0;
        end
        else
        begin
            state_reg           <= state_next;
            CLP_A_In            <= CLP_A_In_Next;
            CLP_B_In            <= CLP_B_In_Next;
            CLP_C_In            <= CLP_C_In_Next;
            CLP_D_In            <= CLP_D_In_Next;
            CONT                <= CONT_Next;
            Byte_MEM_In         <= Byte_MEM_In_Next;
            Prog_Mem_Address    <= Prog_Mem_Address_Next;
            Prog_Mem_Write      <= Prog_Mem_Write_Next;
            Debug_Stall         <= Debug_Stall_Next;
            Mode_Op             <= Mode_Op_Next;
            Data_Out            <= Data_Out_Next;
            CONT2               <= CONT2_Next;
            tx_start_reg        <= tx_start_next;   
        end
    end
    
    always @(*)
    begin
        state_next = state_reg;
        CLP_A_In_Next           = CLP_A_In;
        CLP_B_In_Next           = CLP_B_In;
        CLP_C_In_Next           = CLP_C_In;
        CLP_D_In_Next           = CLP_D_In;
        CONT_Next               = CONT;
        Byte_MEM_In_Next        = Byte_MEM_In;
        Prog_Mem_Address_Next   = Prog_Mem_Address;
        Prog_Mem_Write_Next     = Prog_Mem_Write;
        Debug_Stall_Next        = Debug_Stall;
        Mode_Op_Next            = Mode_Op;
        Data_Out_Next           = Data_Out;
        CONT2_Next              = CONT2;
        tx_start_next           = tx_start_reg;  
        
        case(state_reg)
            CLP1:
            begin
                tx_start_next = 1'b0;
                if(rx_done_tick)
                begin
                    CLP_A_In_Next = MIPS_rx_data;
                    state_next = CLP2;
                end
            end
            CLP2:
            begin
                if(rx_done_tick)
                begin
                    CLP_B_In_Next = MIPS_rx_data;
                    state_next = CLP3;
                end
            end 
            CLP3:
            begin
                if(rx_done_tick)
                begin
                    CLP_C_In_Next = MIPS_rx_data;
                    state_next = CLP4;
                end
            end
            CLP4:
            begin
                if(rx_done_tick)
                begin
                    CLP_D_In_Next = MIPS_rx_data;
                    state_next = MEMA;
                    Prog_Mem_Write_Next = 1'b0;
                end
            end
            MEMA:
            begin
                Prog_Mem_Write_Next = 1'b0;
                if(rx_done_tick)
                begin
                    Byte_MEM_In_Next = MIPS_rx_data;
                    if(Prog_Mem_Address == 0)
                    begin
                        Prog_Mem_Address_Next = 0;
                    end
                    else
                    begin
                        Prog_Mem_Address_Next = Prog_Mem_Address + 1;
                    end
                    Prog_Mem_Write_Next = 1'b1; 
                    state_next = MEMB;
                end
                else
                begin
                    Prog_Mem_Write_Next = 1'b0; 
                end
            end
            MEMB:
            begin
                Prog_Mem_Write_Next = 1'b0;
                if(rx_done_tick)
                begin
                    Byte_MEM_In_Next = MIPS_rx_data;
                    Prog_Mem_Address_Next = Prog_Mem_Address + 1;
                    Prog_Mem_Write_Next = 1'b1; 
                    state_next = MEMC;
                end
                else
                begin
                    Prog_Mem_Write_Next = 1'b0; 
                end
            end
            MEMC:
            begin
                Prog_Mem_Write_Next = 1'b0;
                if(rx_done_tick)
                begin
                    Byte_MEM_In_Next = MIPS_rx_data;
                    Prog_Mem_Address_Next = Prog_Mem_Address + 1;
                    Prog_Mem_Write_Next = 1'b1; 
                    state_next = MEMD;
                end
                else
                begin
                    Prog_Mem_Write_Next = 1'b0; 
                end
            end
            MEMD:
            begin
                Prog_Mem_Write_Next = 1'b0;
                if(rx_done_tick)
                begin
                    Byte_MEM_In_Next = MIPS_rx_data;
                    Prog_Mem_Address_Next = Prog_Mem_Address + 1;
                    Prog_Mem_Write_Next = 1'b0; 
                    if(CONT == CLP - 1)
                    begin
                        state_next = EMOD;
                        Prog_Mem_Write_Next = 1'b1;
                    end
                    else
                    begin
                        state_next = MEMA;
                        CONT_Next  = CONT_Next + 1;
                        Prog_Mem_Write_Next = 1'b1;
                    end 
                end
                else
                begin
                    Prog_Mem_Write_Next = 1'b0; 
                end
            end
            EMOD:
            begin
                Prog_Mem_Write_Next = 1'b0; 
                if(rx_done_tick)
                begin
                    Mode_Op_Next = MIPS_rx_data;
                    state_next = EXEC; 
                    tx_start_next = 1'b0;
                    Debug_Stall_Next = 0;
                end
            end
            EXEC:
            begin
                if((Mode_Op == 0) && (!i_hlt))
                begin
                    state_next = EXEC;
                    tx_start_next = 1'b0;
                    Debug_Stall_Next = 0;
                end
                else
                begin
                    Debug_Stall_Next = 1;
                    state_next = REGA;
                    Data_Out_Next = table2[CONT2][7 : 0];
                    tx_start_next = 1'b1;
                end
            end
            REGA:
            begin
                Data_Out_Next = table2[CONT2][15 : 8];
                if(tx_done_tick)
                begin
                    state_next = REGB;
                    tx_start_next = 1'b1;
                end
            end
            REGB:
            begin
                Data_Out_Next = table2[CONT2][23 : 16];
                if(tx_done_tick)
                begin
                    state_next = REGC;
                end         
            end
            REGC:
            begin
                Data_Out_Next = table2[CONT2][31 : 24];
                if(tx_done_tick)
                begin
                    state_next = REGD;
                end
            end
            REGD:
            begin
                if(tx_done_tick)
                begin
                    if(CONT2 == TC_WORD - 1)
                    begin
                        if(Mode_Op == 0) // Continuous
                        begin
                            state_next = EHLT;
                            tx_start_next = 1'b0;
                        end
                        else if((Mode_Op == 1) && (!i_hlt)) // Step
                        begin
                            tx_start_next = 1'b0;
                            CONT2_Next = 0;
                            state_next = EMOD;      
                        end
                        else
                        begin
                            state_next = EHLT;
                            tx_start_next = 1'b0;
                        end
                    end
                    else
                    begin
                        CONT2_Next = CONT2 + 1;
                        state_next = REGA;
                        tx_start_next = 1'b1;
                        Data_Out_Next = table2[CONT2 + 1][7 : 0];
                    end
                end
            end
            EHLT:
            begin
                tx_start_next = 1'b0;
                state_next = EHLT;
            end
            default
            begin
                state_next = 4'b0000;
            end
        endcase
    end

UART
#( 
    .NB_UART            (NB_UART),    // Number of data bits
    .SB_TICK            (SB_TICK), 
    .NB_CONT            (NB_CONT),
    .NB_BRCV            (NB_BRCV),
    .M                  (M)        // Parameter for Baud Rate Generator
 )
UART_1
(
    .i_clk              (i_clk), 
    .i_Reset            (i_Reset),
    .i_rx               (i_rx),
    .i_tx_data          (Data_Out),
    .i_tx_start         (tx_start_reg),
    
    .o_tx               (o_tx),
    .o_rx_done_tick     (rx_done_tick),
    .o_tx_done_tick     (tx_done_tick),
    .o_rx_data          (MIPS_rx_data)
);

endmodule
