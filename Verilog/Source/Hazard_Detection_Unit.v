`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01.06.2022 09:12:56
// Design Name: 
// Module Name: Hazard_Detection_Unit
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Hazard_Detection_Unit
#(
    parameter   NB_RDIR     = 5   
 )
 (
    // Inputs
    input   wire                            i_Mem_Read_EX,
    input   wire    [NB_RDIR    - 1 : 0]    i_rs_ID,
    input   wire    [NB_RDIR    - 1 : 0]    i_rt_ID,
    input   wire    [NB_RDIR    - 1 : 0]    i_rt_EX,
    
    // Output
    output  wire                            o_Stall
 );
 
    // Detects hazard condition and asserts stall signal if needed
    assign o_Stall = (i_Mem_Read_EX && ((i_rt_EX == i_rs_ID) || (i_rt_EX == i_rt_ID)));
 
endmodule
