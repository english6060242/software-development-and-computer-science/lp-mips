`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01.06.2022 09:15:41
// Design Name: 
// Module Name: UART
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module UART
#( 
    parameter   NB_UART = 8,    // Number of data bits
    parameter   SB_TICK = 16, 
    parameter   NB_CONT = 4,
    parameter   NB_BRCV = 3,
    parameter   M = 14        // Parameter for Baud Rate Generator
)
(
    input   wire                                    i_clk, i_Reset,
    input   wire                                    i_rx,
    input   wire              [NB_UART  - 1 : 0]    i_tx_data,
    input   wire                                    i_tx_start,
    
    output  wire                                    o_tx,
    output  wire                                    o_rx_done_tick,
    output  wire                                    o_tx_done_tick,
    output  wire              [NB_UART  - 1 : 0]    o_rx_data
);
  
    // Internal
    wire                                tick;
    
    
Baud_Rate_Generator
#(
    .M                  (M)
)
u_Baud_rate_Gen_1
(
    // Inputs
    .i_clk              (i_clk),
    .i_Reset            (i_Reset),

    // Outputs
    .s_tick             (tick)
);

UART_RX
#(
    .NB_UART            (NB_UART),    // Number of data bits
    .SB_TICK            (SB_TICK),   // Number of ticks necessary for stop bits
    .NB_STAT            (2),    // Number of bits to represent states    
    .NB_CONT            (NB_CONT),
    .NB_BRCV            (NB_BRCV)
)
u_UART_RX_1
(
    // Inputs
    .i_clk              (i_clk),
    .i_Reset            (i_Reset),        
    .i_rx               (i_rx),           // rx
    .s_tick             (tick),         // Baud Rate Gen signal
    
    // Outputs
    .rx_done_tick       (o_rx_done_tick),   // Indicates that the data has been received
    .dout               (o_rx_data)
);

UART_TX
#(
    .NB_UART            (NB_UART),    // Number of data bits
    .SB_TICK            (SB_TICK),   // Number of ticks necessary for stop bits
    .NB_STAT            (2),    // Number of bits to represent states    
    .NB_CONT            (NB_CONT),
    .NB_BRCV            (NB_BRCV)
)
u_UART_TX_1
(
    // Inputs
    .i_clk              (i_clk),
    .i_Reset            (i_Reset),        
    .i_tx_start         (i_tx_start),           
    .s_tick             (tick),         // Baud Rate Gen signal
    .i_din              (i_tx_data),
    
    // Outputs
    .o_tx_done_tick     (o_tx_done_tick),
    .o_tx               (o_tx)
);
    
endmodule
