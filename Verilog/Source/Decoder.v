`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01.06.2022 09:10:14
// Design Name: 
// Module Name: Decoder
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Decoder
#(
    parameter   OP_CODE = 6,
    parameter   NB_FUNC = 6,
    parameter   ALU_OPB = 4,
    parameter   NB_2BIT = 2,
    parameter   ALU_DEC = 3
 )
 (
    // Inputs
    input   wire    [OP_CODE - 1   : 0]    i_Opcode,
    input   wire    [NB_FUNC - 1   : 0]    i_Function,

    // Outputs
    output  wire                                        o_Jump,
    output  wire                                        o_Jump_Register,
    output  wire                                        o_Branch,
    output  wire                                        o_On_Equal,
    output  wire                                        o_Mem_Read,
    output  wire                                        o_Mem_Write,
    output  wire                [NB_2BIT - 1 : 0]       o_BHW,
    output  wire                                        o_Signed_Unsigned,
    output  wire                [NB_2BIT - 1 : 0]       o_Mem_To_Reg,
    output  wire                                        o_Write_Reg,
    output  wire                [NB_2BIT - 1 : 0]       o_Reg_Dest,
    output  wire                [ALU_OPB - 1 : 0]       o_ALU_Op,
    output  wire                                        o_ALU_Src,
    output  wire                                        o_Halt
 );
 
    // Opcode
    localparam RTYPE = 6'b000000;
    localparam LB    = 6'b100000;
    localparam LH    = 6'b100001;
    localparam LW    = 6'b100011;
    localparam LWU   = 6'b100111;
    localparam LBU   = 6'b100100;
    localparam LHU   = 6'b100101;
    localparam SB    = 6'b101000;
    localparam SH    = 6'b101001;
    localparam SW    = 6'b101011;
    localparam ADDI  = 6'b001000;
    localparam ANDI  = 6'b001100;
    localparam ORI   = 6'b001101;
    localparam XORI  = 6'b001110;
    localparam LUI   = 6'b001111;
    localparam SLTI  = 6'b001010;
    localparam BEQ   = 6'b000100;
    localparam BNE   = 6'b000101;
    localparam J     = 6'b000010;
    localparam JAL   = 6'b000011;
    localparam HLT   = 6'b111111;
    
    // R-type function (either R type or not R type but with Opcode = 6b'000000)
    localparam R_SLL  = 6'b000000;
    localparam R_SRL  = 6'b000010;
    localparam R_SRA  = 6'b000011;
    localparam R_SLLV = 6'b000100;
    localparam R_SRLV = 6'b000110;
    localparam R_SRAV = 6'b000111;
    localparam R_ADDU = 6'b100001;
    localparam R_SUBU = 6'b100011;
    localparam R_AND  = 6'b100100;
    localparam R_OR   = 6'b100101;
    localparam R_XOR  = 6'b100110;
    localparam R_NOR  = 6'b100111;
    localparam R_SLT  = 6'b101010;
    localparam R_JR   = 6'b001000;
    localparam R_JALR = 6'b001001; // JALR
    
    // ALU Operation
    localparam OP_SLL  = 4'b0000;
    localparam OP_SRL  = 4'b0001;
    localparam OP_SRA  = 4'b0010;
    localparam OP_SLLV = 4'b0011;
    localparam OP_SRLV = 4'b0100;
    localparam OP_SRAV = 4'b0101;
    localparam OP_ADDU = 4'b0110;
    localparam OP_SUBU = 4'b0111;
    localparam OP_AND  = 4'b1000;
    localparam OP_OR   = 4'b1001;
    localparam OP_XOR  = 4'b1010;
    localparam OP_NOR  = 4'b1011;
    localparam OP_SLT  = 4'b1100;
    localparam OP_JR   = 4'bxxxx;
    localparam OP_JALR = 4'b1101;
    localparam OP_LUI  = 4'b1110;
    
    reg                         Jump;
    reg                         Jump_Register;
    reg                         Branch;
    reg                         On_Equal;
    reg                         Mem_Read;
    reg                         Mem_Write;
    reg     [NB_2BIT - 1 : 0]   BHW;
    reg                         Signed_Unsigned;
    reg     [NB_2BIT - 1 : 0]   Mem_To_Reg;
    reg                         Write_Reg;
    reg     [NB_2BIT - 1 : 0]   Reg_Dest;
    //reg                         PC_Write;
    reg     [ALU_OPB - 1 : 0]   ALU_Op;     
    reg                         ALU_Src;
    reg                         Halt;
    
    assign o_Jump            = Jump; 
    assign o_Jump_Register   = Jump_Register; 
    assign o_Branch          = Branch;
    assign o_On_Equal        = On_Equal;
    assign o_Mem_Read        = Mem_Read;
    assign o_Mem_Write       = Mem_Write;
    assign o_BHW             = BHW;
    assign o_Signed_Unsigned = Signed_Unsigned;
    assign o_Mem_To_Reg      = Mem_To_Reg;
    assign o_Write_Reg       = Write_Reg;
    assign o_Reg_Dest        = Reg_Dest;
    assign o_ALU_Op          = ALU_Op;
    assign o_ALU_Src         = ALU_Src;
    assign o_Halt            = Halt; 

    always @(*) 
    begin
    //begin : proc_Op
        case(i_Opcode)    
            RTYPE: 
            begin
                Signed_Unsigned   = 1'bX;
                Reg_Dest          = 2'b01;
                ALU_Src           = 1'b0;
                Branch            = 1'b0;
                On_Equal          = 1'b0;
                Mem_Read          = 1'b0;
                Mem_Write         = 1'b0;
                Jump              = 1'b0;
                Jump_Register     = 1'b0;
                Halt              = 1'b0;
                
                case(i_Function)
                    R_SLL: ALU_Op   = OP_SLL;
                    R_SRL: ALU_Op   = OP_SRL;
                    R_SRA: ALU_Op   = OP_SRA;
                    R_SLLV: ALU_Op  = OP_SLLV;
                    R_SRLV: ALU_Op  = OP_SRLV;
                    R_SRAV: ALU_Op  = OP_SRAV;
                    R_ADDU: ALU_Op  = OP_ADDU;
                    R_SUBU: ALU_Op  = OP_SUBU;
                    R_AND: ALU_Op   = OP_AND;
                    R_OR: ALU_Op    = OP_OR;
                    R_XOR: ALU_Op   = OP_XOR;
                    R_NOR: ALU_Op   = OP_NOR;
                    R_SLT: ALU_Op   = OP_SLT;
                    R_JR: ALU_Op    = OP_JR;
                    R_JALR: ALU_Op  = OP_JALR;
                    default: ALU_Op = 4'bxxxx;
                endcase 
            end
            
            ADDI: 
            begin
                Signed_Unsigned   = 1'b1;
                Reg_Dest          = 2'b01;
                ALU_Src           = 1'b1;
                Branch            = 1'b0;
                On_Equal          = 1'b0;
                Mem_Read          = 1'b0;
                Mem_Write         = 1'b0;
                Jump              = 1'b0;
                Jump_Register     = 1'b0;
                Halt              = 1'b0;
                ALU_Op            = OP_ADDU;
            end

            LW, LH, LHU, LWU:
            begin
                Signed_Unsigned   = 1'b1;
                Reg_Dest          = 2'b01;
                ALU_Src           = 1'b1;
                Branch            = 1'b0;
                On_Equal          = 1'b0;
                Mem_Read          = 1'b1;
                Mem_Write         = 1'b0;
                Jump              = 1'b0;
                Jump_Register     = 1'b0;
                Halt              = 1'b0;
                ALU_Op            = OP_ADDU;
            end

            SW, SH, SB:
            begin
                Signed_Unsigned   = 1'bX;
                Reg_Dest          = 2'bXX;
                ALU_Src           = 1'b1;
                Branch            = 1'b0;
                On_Equal          = 1'b0;
                Mem_Read          = 1'b0;
                Mem_Write         = 1'b1;
                Jump              = 1'b0;
                Jump_Register     = 1'b0;
                Halt              = 1'b0;
                ALU_Op            = OP_ADDU;
            end

            BEQ, BNE:
            begin
                Signed_Unsigned   = 1'bX;
                Reg_Dest          = 2'bXX;
                ALU_Src           = 1'b0;
                Branch            = 1'b1;
                On_Equal          = (i_Opcode == BEQ) ? 1'b1 : 1'b0;
                Mem_Read          = 1'b0;
                Mem_Write         = 1'b0;
                Jump              = 1'b0;
                Jump_Register     = 1'b0;
                Halt              = 1'b0;
                ALU_Op            = OP_SUBU;
            end

            J, JAL:
            begin
                Signed_Unsigned   = 1'bX;
                Reg_Dest          = 2'bXX;
                ALU_Src           = 1'bX;
                Branch            = 1'b0;
                On_Equal          = 1'b0;
                Mem_Read          = 1'b0;
                Mem_Write         = 1'b0;
                Jump              = (i_Opcode == JAL) ? 1'b1 : 1'b0;
                Jump_Register     = (i_Opcode == JAL) ? 1'b1 : 1'b0;
                Halt              = 1'b0;
                ALU_Op            = 4'bxxxx;
            end

            default: //HLT
            begin
                Signed_Unsigned   = 1'bX;
                Reg_Dest          = 2'bXX;
                ALU_Src           = 1'bX;
                Branch            = 1'b0;
                On_Equal          = 1'bX;
                Mem_Read          = 1'bX;
                Mem_Write         = 1'bX;
                Jump              = 1'bX;
                Jump_Register     = 1'bX;
                Halt              = 1'b1;
                ALU_Op            = 4'bxxxx;
            end
        endcase
    end

endmodule
