`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01.06.2022 09:11:57
// Design Name: 
// Module Name: Shift_Left_2_28
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Shift_Left_2_28
#(
    parameter   NB_DATA_IN   = 26,
    parameter   NB_DATA_OUT = 28
 )
 (
    // Inputs
    input   wire    [NB_DATA_IN  - 1 : 0]    i_SL228_IN,
    // Outputs
    output  wire    [NB_DATA_OUT - 1 : 0]    o_SL228_OUT
 );

    assign o_SL228_OUT = i_SL228_IN << 2;

endmodule
