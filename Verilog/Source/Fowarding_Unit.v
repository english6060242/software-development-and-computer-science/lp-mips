`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01.06.2022 09:13:50
// Design Name: 
// Module Name: Fowarding_Unit
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Fowarding_Unit
#(
    parameter   NB_RDIR     = 5,
    parameter   NB_MUX4     = 2
 )
 (
    // Inputs
    input   wire                            i_Write_Reg_MEM,
    input   wire                            i_Write_Reg_WB,
    input   wire                            i_Mem_Write_EX,
    input   wire                            i_Branch_ID,
    input   wire                            i_Jump_Register_ID,
    input   wire    [NB_RDIR    - 1 : 0]    i_rs_ID,
    input   wire    [NB_RDIR    - 1 : 0]    i_rs_EX,
    input   wire    [NB_RDIR    - 1 : 0]    i_rt_ID,
    input   wire    [NB_RDIR    - 1 : 0]    i_rt_EX,
    input   wire    [NB_RDIR    - 1 : 0]    i_Dest_Register_MEM,
    input   wire    [NB_RDIR    - 1 : 0]    i_Dest_Register_WB,
    
    // Outputs
    output  wire    [NB_MUX4    - 1 : 0]    o_F1,
    output  wire    [NB_MUX4    - 1 : 0]    o_F2,
    output  wire    [NB_MUX4    - 1 : 0]    o_F3,
    output  wire    [NB_MUX4    - 1 : 0]    o_F4,
    output  wire    [NB_MUX4    - 1 : 0]    o_F5,
    output  wire    [NB_MUX4    - 1 : 0]    o_F6
 );
 
    reg             [NB_MUX4    - 1 : 0]    F1;
    reg             [NB_MUX4    - 1 : 0]    F2;
    reg             [NB_MUX4    - 1 : 0]    F3;
    reg             [NB_MUX4    - 1 : 0]    F4;
    reg             [NB_MUX4    - 1 : 0]    F5;
    reg             [NB_MUX4    - 1 : 0]    F6;

    
    assign  o_F1 = F1;
    assign  o_F2 = F2;
    assign  o_F3 = F3;
    assign  o_F4 = F4;
    assign  o_F5 = F5;
    assign  o_F6 = F6;

    always@(*) 
    begin
        if(i_Write_Reg_MEM && (i_Dest_Register_MEM == i_rs_EX))
        begin
            F1 = 2'b10; // ALU_Result_MEM
        end
        else if (i_Write_Reg_WB && (i_Dest_Register_WB == i_rs_EX))
        begin
            F1 = 2'b01; // Reg_Data_In_WB
        end
        else 
        begin
            F1 = 2'b00;
        end
    end

    always@(*)
    begin
        if(i_Write_Reg_MEM && (i_Dest_Register_MEM == i_rt_EX))
        begin
            F2 = 2'b10; // ALU_Result_MEM
        end

        else if (i_Write_Reg_WB && (i_Dest_Register_WB == i_rt_EX))
        begin
            F2 = 2'b01; // Reg_Data_In_WB
        end
        else
        begin 
            F2 = 2'b00;
        end
    end
    
    always@(*)
    begin
        
        if (i_Mem_Write_EX && (i_Dest_Register_MEM == i_rt_EX) && i_Write_Reg_MEM)
        begin 
            F5 = 2'b10; // ALU_Result_MEM
        end

        else if (i_Mem_Write_EX && (i_Dest_Register_WB == i_rt_EX) && i_Write_Reg_WB)
        begin
            F5 = 2'b01; // Reg_Data_In_WB
        end
        else
        begin 
            F5 = 2'b00;
        end
    end
    
    
    always@(*)
    begin
        if(i_Branch_ID && (i_Dest_Register_MEM == i_rs_ID) && i_Write_Reg_MEM)
        begin
            F3 = 2'b10; // ALU_Result_MEM
        end
        else if (i_Write_Reg_WB && (i_Dest_Register_WB == i_rs_ID) && i_Branch_ID) 
        begin
            F3 = 2'b01; // Reg_Data_In_WB
        end
        else
        begin
            F3 = 2'b00;
        end
    end
    
    always@(*)
    begin
        if(i_Branch_ID && (i_Dest_Register_MEM == i_rt_ID) && i_Write_Reg_MEM)
        begin
            F4 = 2'b10; // ALU_Result_MEM
        end
        else if(i_Write_Reg_WB && (i_Dest_Register_WB == i_rt_ID) && i_Branch_ID)
        begin
            F4 = 2'b01; // Reg_Data_In_WB
        end
        else
        begin
            F4 = 2'b00;
        end
    end

    always@(*)
    begin
        if (i_Jump_Register_ID && (i_Dest_Register_MEM == i_rs_ID) && i_Write_Reg_MEM)
        begin
            F6 = 2'b10; // ALU_Result_MEM
        end
        else if (i_Jump_Register_ID && (i_Dest_Register_WB == i_rs_ID) && i_Write_Reg_WB)
        begin 
            F6 = 2'b01; // Reg_Data_In_WB
        end
        else
        begin 
            F6 = 2'b00; 
        end 
    end

endmodule
