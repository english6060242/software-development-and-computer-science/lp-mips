`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01.06.2022 11:43:51
// Design Name: 
// Module Name: Isolated_MIPS
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Isolated_MIPS
#(
    parameter   NB_DATA     = 32,
    parameter   NB_UART     = 8,
    parameter   NB_DIR_PC   = 32,
    parameter   NB_ALUOP    = 4,
    parameter   NB_SHAMT    = 5,
    parameter   NB_OPCODE   = 6,
    parameter   NB_FUNC     = 6,
    parameter   NB_RDIR     = 5,     // Register address bits
    parameter   NB_MUX4     = 2,
    parameter   NB_SEND     = 384,   // Number of bits to send (table)
    parameter   NB_ALLR     = 1024,  // Number of bits of registers to send
    parameter   NB_DMEM     = 320    // Number of bits of data memory to send
 )
(
    // Inputs
    input   wire                          i_clk,                  // Clock
    input   wire                          i_Reset,                // Reset
    input   wire                          i_Reset_clk,  
    input   wire                          i_Stall,
    
    input   wire    [NB_DATA  - 1 : 0]    i_Prog_Write_Address,
    input   wire    [NB_UART  - 1 : 0]    i_Prog_MEM_Data,
    input   wire                          i_Prog_MEM_Write,
    
    // Outputs
    output  wire                          o_HLT,
    output  wire    [NB_SEND  - 1 : 0]    o_table,
    output  wire                          o_locked
 );

wire                            clk_out_1;

// Internal
wire                            Halt;
wire                            Stall;
wire                            hd_Stall;

wire    [NB_DIR_PC - 1 : 0] PC_In;  // Next program memory address
wire    [NB_DIR_PC - 1 : 0] PC_Out;    // PC output indicates the address to read from program memory

// Program Memory
wire    [NB_DATA - 1 : 0]   Instruction_IF;
wire    [NB_DATA - 1 : 0]   Instruction_ID;

// Adder
wire    [NB_DATA - 1 : 0]   PC_Plus_Four_IF;
wire    [NB_DATA - 1 : 0]   PC_Plus_Four_ID;
wire    [NB_DATA - 1 : 0]   PC_Plus_Four_EX;
wire    [NB_DATA - 1 : 0]   PC_Plus_Four_MEM;
wire    [NB_DATA - 1 : 0]   PC_Plus_Four_WB;

// MUX 1
wire    [NB_DATA - 1 : 0]   Next_Or_Branch;  // Instruction [15 - 0] << 2

// MUX 2
wire    [NB_DATA - 1 : 0]   NextOrBranch_Or_Jump;  // Instruction [15 - 0] << 2	

// Decoder
wire                                Jump_ID;
wire                                Jump_Register_ID;
wire                                Branch_ID;
wire                                On_Equal_ID;
wire                                PC_Write_ID;
wire                                Mem_Read_ID;
wire                                Mem_Write_ID;
wire                                Signed_ID;
wire    [NB_MUX4    - 1 : 0]        BHW_ID;
wire                                Write_Reg_ID;
wire    [NB_MUX4    - 1 : 0]        Mem_To_Reg_ID;
wire    [NB_MUX4    - 1 : 0]        Reg_Dest_ID;
wire                                ALU_Src_ID;
//wire    [NB_DECO    - 1 : 0]        ALU_Dec_Op_ID;
wire                                Halt_ID;

wire                                Jalr_ID;

wire    [NB_FUNC  - 1 : 0]          Function_EX; 
wire                                Jump_Register_EX;

wire                                Mem_Read_EX;
wire                                Mem_Write_EX;
wire                                Signed_EX;
wire    [NB_MUX4    - 1 : 0]        BHW_EX;
wire                                Write_Reg_EX;
wire    [NB_MUX4    - 1 : 0]        Mem_To_Reg_EX;
wire    [NB_MUX4    - 1 : 0]        Reg_Dest_EX;
wire                                ALU_Src_EX;

wire    [NB_ALUOP   - 1 : 0]        ALU_Operation_ID;
wire    [NB_ALUOP   - 1 : 0]        ALU_Operation_EX;

wire    [NB_SHAMT   - 1 : 0]        Shamt_ID;
wire    [NB_SHAMT   - 1 : 0]        Shamt_EX;

wire    [NB_OPCODE  - 1 : 0]        Opcode_IF;
wire    [NB_OPCODE  - 1 : 0]        Opcode_ID;
wire    [NB_OPCODE  - 1 : 0]        Opcode_EX;
wire    [NB_OPCODE  - 1 : 0]        Opcode_MEM;
wire    [NB_OPCODE  - 1 : 0]        Opcode_WB;

wire    [NB_RDIR    - 1 : 0]        Read_Register_1_ID;
wire    [NB_RDIR    - 1 : 0]        Read_Register_2_ID;
wire    [NB_RDIR    - 1 : 0]        Read_Register_1_EX;
wire    [NB_RDIR    - 1 : 0]        Read_Register_2_EX;

wire    [NB_RDIR    - 1 : 0]        Write_Register_ID;
wire    [NB_RDIR    - 1 : 0]        Write_Register_EX;

wire    [NB_ALLR    - 1 : 0]        Register_Data_1_ID;
wire    [NB_ALLR    - 1 : 0]        Register_Data_2_ID;
wire    [NB_ALLR    - 1 : 0]        Register_Data_1_EX;
wire    [NB_ALLR    - 1 : 0]        Register_Data_2_EX;

wire    [NB_DATA    - 1 : 0]        Sign_Extended_ID;
wire    [NB_DATA    - 1 : 0]        Sign_Extended_EX;

wire    [NB_DATA    - 1 : 0]        ALU_Output_EX;
wire    [NB_DATA    - 1 : 0]        ALU_Output_MEM;
wire    [NB_DATA    - 1 : 0]        ALU_Output_WB;

wire    [NB_DATA    - 1 : 0]        Mux_Data_EX;
wire    [NB_DATA    - 1 : 0]        Mux_Data_MEM;
wire    [NB_DATA    - 1 : 0]        Mux_Data_WB;

wire    [NB_DMEM    - 1 : 0]        Data_Mem_Read;
wire    [NB_DMEM    - 1 : 0]        Data_Mem_Write;

wire                                Write_Register_WB;
wire    [NB_DMEM    - 1 : 0]        Data_Write_WB;

assign  Stall = (i_Stall) ? 1'b1 : hd_Stall;
assign  o_HLT = Halt;
assign  o_table = { Data_Write_WB, Write_Register_WB, ALU_Output_WB, Write_Reg_EX, BHW_EX, Shamt_EX, ALU_Operation_EX, Mem_To_Reg_EX, Function_EX };
assign  o_locked = i_Reset_clk;

always @(posedge i_clk)
begin
    if(i_Reset)
    begin
        hd_Stall <= 1'b0;
    end
    else if(Stall)
    begin
        hd_Stall <= 1'b1;
    end
    else
    begin
        hd_Stall <= 1'b0;
    end
end


mux4  U6 (
    .in0 (Register_Data_1_ID),
    .in1 (Register_Data_2_ID),
    .in2 (Sign_Extended_ID),
    .in3 (PC_Plus_Four_ID),
    .sel (BHW_ID),
    .out (Mux_Data_EX)
);

mux4  U7 (
    .in0 (Register_Data_1_EX),
    .in1 (ALU_Output_EX),
    .in2 (Data_Mem_Read),
    .in3 (Data_Mem_Write),
    .sel (Mem_To_Reg_EX),
    .out (Mux_Data_MEM)
);

mux4  U8 (
    .in0 (ALU_Output_MEM),
    .in1 (ALU_Output_WB),
    .in2 (Mux_Data_MEM),
    .in3 (Mux_Data_WB),
    .sel (Write_Reg_EX),
    .out (Mux_Data_WB)
);

// Multiplexer 4_1 for WB instruction
mux4  U9 (
    .in0 (Mux_Data_WB),
    .in1 (Mux_Data_MEM),
    .in2 (Mux_Data_EX),
    .in3 (Instruction_ID[15:0]),
    .sel (ALU_Src_ID),
    .out (Mux_Data_WB)
);

// Multiplexer 2_1
mux2_1  U10 (
    .in0 (NextOrBranch_Or_Jump),
    .in1 (PC_Plus_Four_IF),
    .sel (Jump_ID),
    .out (PC_In)
);

// Multiplexer 4_1 for EX
mux4  U11 (
    .in0 (Sign_Extended_EX),
    .in1 (ALU_Output_EX),
    .in2 (Data_Mem_Read),
    .in3 (Data_Mem_Write),
    .sel (Mem_To_Reg_EX),
    .out (Mux_Data_MEM)
);

// Multiplexer 4_1 for MEM
mux4  U12 (
    .in0 (ALU_Output_MEM),
    .in1 (ALU_Output_WB),
    .in2 (Mux_Data_MEM),
    .in3 (Mux_Data_WB),
    .sel (Write_Reg_EX),
    .out (Mux_Data_WB)
);

// Multiplexer 4_1 for WB instruction
mux4  U13 (
    .in0 (Mux_Data_WB),
    .in1 (Mux_Data_MEM),
    .in2 (Mux_Data_EX),
    .in3 (Instruction_ID[15:0]),
    .sel (ALU_Src_ID),
    .out (Mux_Data_WB)
);

// Multiplexer 2_1
mux2_1  U14 (
    .in0 (NextOrBranch_Or_Jump),
    .in1 (PC_Plus_Four_IF),
    .sel (Jump_ID),
    .out (PC_In)
);

// Register File
RegFile U15 (
    .clk (i_clk),
    .rst (i_Reset),
    .Read_Register_1 (Read_Register_1_ID),
    .Read_Register_2 (Read_Register_2_ID),
    .Write_Register (Write_Register_ID),
    .Write_Data (Mux_Data_WB),
    .Data_Read_1 (Register_Data_1_ID),
    .Data_Read_2 (Register_Data_2_ID),
    .Write_Enable (Write_Reg_ID)
);

// ALU
alu     U16 (
    .a (Register_Data_1_EX),
    .b (Mux_Data_EX),
    .op (ALU_Operation_EX),
    .result (ALU_Output_EX),
    .shamt (Shamt_EX),
    .func (Function_EX),
    .Signed (Signed_EX)
);

// Data Memory
mem     U17 (
    .clk (i_clk),
    .wen (MemWrite),
    .ren (MemRead),
    .addr (ALU_Output_EX),
    .data_write (Register_Data_2_EX),
    .data_read (Data_Mem_Read)
);

// Branch Address Calculation
branch_address   U18 (
    .Instruction (Instruction_ID),
    .PC_Plus_Four (PC_Plus_Four_ID),
    .Read_Data_1 (Register_Data_1_ID),
    .Read_Data_2 (Register_Data_2_ID),
    .Branch (Branch)
);

// Jump Address Calculation
jump_address     U19 (
    .Instruction (Instruction_ID),
    .PC_Plus_Four (PC_Plus_Four_ID),
    .Read_Data_1 (Register_Data_1_ID),
    .Sign_Extended (Sign_Extended_ID),
    .Jump (Jump)
);

// ID - Instruction Decode
ID      U20 (
    .i_clk (i_clk),
    .i_reset (i_Reset),
    .i_hlt (i_HLT),
    .i_Instruction (Instruction_ID),
    .i_PC (PC_ID),
    .i_register_rs1 (Read_Register_1_ID),
    .i_register_rs2 (Read_Register_2_ID),
    .i_register_rd (Write_Register_ID),
    .i_MemRead (MemRead_ID),
    .i_MemWrite (MemWrite_ID),
    .o_Opcode (Opcode_ID),
    .o_Read_Register_1 (Read_Register_1_EX),
    .o_Read_Register_2 (Read_Register_2_EX),
    .o_Write_Register (Write_Register_EX),
    .o_RegWrite (Write_Reg_EX),
    .o_MemToReg (Mem_To_Reg_EX),
    .o_BHW (BHW_EX),
    .o_ShiftAmt (Shamt_EX),
    .o_ALU_Operation (ALU_Operation_EX),
    .o_Signed (Signed_EX),
    .o_Function (Function_EX),
    .o_Stall (Stall)
);

// IF - Instruction Fetch
IF      U21 (
    .i_clk (i_clk),
    .i_reset (i_Reset),
    .i_stall (Stall),
    .o_Instruction (Instruction_ID),
    .o_PC (PC_ID)
);

// EX - Execute
EX      U22 (
    .i_clk (i_clk),
    .i_reset (i_Reset),
    .i_stall (Stall),
    .i_opcode (Opcode_EX),
    .i_alu_op (ALU_Operation_EX),
    .i_signed (Signed_EX),
    .i_func (Function_EX),
    .i_shamt (Shamt_EX),
    .i_read_data_1 (Register_Data_1_EX),
    .i_read_data_2 (Register_Data_2_EX),
    .i_read_register_1 (Read_Register_1_EX),
    .i_read_register_2 (Read_Register_2_EX),
    .i_Mux_Data (Mux_Data_EX),
    .o_Alu_Result (ALU_Output_EX),
    .o_Branch (Branch),
    .o_MemRead (MemRead_EX),
    .o_MemWrite (MemWrite_EX),
    .o_MemToReg (Mem_To_Reg_EX),
    .o_Write_Register (Write_Register_WB),
    .o_Write_Data (Data_Write_WB),
    .o_NextOrBranch_Or_Jump (NextOrBranch_Or_Jump),
    .o_Signed_Imm (Sign_Extended_EX),
    .o_pc_plus_four (PC_Plus_Four_EX),
    .o_Stall (Stall_EX)
);

// MEM - Memory Access
MEM     U23 (
    .i_clk (i_clk),
    .i_reset (i_Reset),
    .i_stall (Stall),
    .i_opcode (Opcode_MEM),
    .i_Mux_Data (Mux_Data_MEM),
    .i_read_data (Data_Mem_Read),
    .i_write_data (Register_Data_2_EX),
    .i_write_register (Write_Register_WB),
    .i_write_enable (Write_Reg_WB),
    .o_Alu_Result (ALU_Output_MEM)
);

// WB - Write Back
WB      U24 (
    .i_clk (i_clk),
    .i_reset (i_Reset),
    .i_stall (Stall),
    .i_opcode (Opcode_WB),
    .i_Mux_Data (Mux_Data_WB),
    .i_write_register (Write_Register_WB),
    .i_write_enable (Write_Reg_WB),
    .o_Alu_Result (ALU_Output_WB)
);

endmodule

module control_unit (input wire [15:0] Instruction_ID,
                            input wire MemRead_ID,
                            input wire MemWrite_ID,
                            output reg [2:0] Opcode_ID,
                            output reg [NB_SHAMT - 1:0] Shamt_ID,
                            output reg [NB_ALLR - 1:0] Read_Register_1_ID,
                            output reg [NB_ALLR - 1:0] Read_Register_2_ID,
                            output reg [NB_ALLR - 1:0] Write_Register_ID,
                            output reg MemToReg_ID,
                            output reg MemWrite_ID,
                            output reg MemRead_ID,
                            output reg RegWrite_ID,
                            output reg Branch_ID,
                            output reg Jump_ID);

always @(*) begin
        // Extracting opcode
        Opcode_ID <= Instruction_ID[15:13];

        // Decoding Function
        case (Opcode_ID)
            3'b000 : begin // R-type
                        Shamt_ID <= Instruction_ID[10:6];
                        Read_Register_1_ID <= Instruction_ID[12:8];
                        Read_Register_2_ID <= Instruction_ID[7:3];
                        Write_Register_ID <= Instruction_ID[12:8];
                        MemToReg_ID <= 1'b0;
                        MemWrite_ID <= 1'b0;
                        MemRead_ID <= 1'b0;
                        RegWrite_ID <= 1'b1;
                        Branch_ID <= 1'b0;
                        Jump_ID <= 1'b0;
            end
            3'b100 : begin // beq
                        Shamt_ID <= 5'b0;
                        Read_Register_1_ID <= Instruction_ID[12:8];
                        Read_Register_2_ID <= Instruction_ID[7:3];
                        Write_Register_ID <= 5'b0;
                        MemToReg_ID <= 1'b0;
                        MemWrite_ID <= 1'b0;
                        MemRead_ID <= 1'b0;
                        RegWrite_ID <= 1'b0;
                        Branch_ID <= 1'b1;
                        Jump_ID <= 1'b0;
            end
            3'b101 : begin // jump
                        Shamt_ID <= 5'b0;
                        Read_Register_1_ID <= 5'b0;
                        Read_Register_2_ID <= 5'b0;
                        Write_Register_ID <= 5'b0;
                        MemToReg_ID <= 1'b0;
                        MemWrite_ID <= 1'b0;
                        MemRead_ID <= 1'b0;
                        RegWrite_ID <= 1'b0;
                        Branch_ID <= 1'b0;
                        Jump_ID <= 1'b1;
            end
            3'b111 : begin // lui
                        Shamt_ID <= Instruction_ID[15:11];
                        Read_Register_1_ID <= 5'b0;
                        Read_Register_2_ID <= 5'b0;
                        Write_Register_ID <= Instruction_ID[12:8];
                        MemToReg_ID <= 1'b0;
                        MemWrite_ID <= 1'b0;
                        MemRead_ID <= 1'b0;
                        RegWrite_ID <= 1'b1;
                        Branch_ID <= 1'b0;
                        Jump_ID <= 1'b0;
            end
            default : begin
                        Shamt_ID <= 5'b0;
                        Read_Register_1_ID <= 5'b0;
                        Read_Register_2_ID <= 5'b0;
                        Write_Register_ID <= 5'b0;
                        MemToReg_ID <= 1'b0;
                        MemWrite_ID <= 1'b0;
                        MemRead_ID <= 1'b0;
                        RegWrite_ID <= 1'b0;
                        Branch_ID <= 1'b0;
                        Jump_ID <= 1'b0;
            end
        endcase
    end

endmodule

