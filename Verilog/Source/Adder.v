`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01.06.2022 09:09:15
// Design Name: 
// Module Name: Adder
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Adder
#(
    parameter   NB_DATA = 32
 )
 (
    // Inputs
    input   wire    [NB_DATA - 1 : 0]    i_Adder_In,
    // Outputs
    output  wire    [NB_DATA - 1 : 0]    o_Adder_Out
 );
 
 assign o_Adder_Out = i_Adder_In + 4;

endmodule
