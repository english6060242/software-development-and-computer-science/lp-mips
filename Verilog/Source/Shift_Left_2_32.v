`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01.06.2022 09:11:15
// Design Name: 
// Module Name: Shift_Left_2_32
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Shift_Left_2_32
#(
    parameter   NB_DATA   = 32
)
(
    // Inputs
    input   wire    [NB_DATA - 1 : 0]    i_SL232_IN,
    // Outputs
    output  wire    [NB_DATA - 1 : 0]    o_SL232_OUT
 );

    assign o_SL232_OUT = i_SL232_IN << 2;
  
endmodule
