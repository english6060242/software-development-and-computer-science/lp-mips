`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01.06.2022 09:07:49
// Design Name: 
// Module Name: PC
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module PC
#(
    parameter   NB_DIR_PC = 32
 )
 (
    // Inputs
    input   wire                       i_clk,
	input   wire                       i_Reset,
	input   wire                       i_Stall,
	input   wire                       i_Halt,     // Halt
	input   wire [NB_DIR_PC - 1 : 0]   i_PC_In,  // Next address from program memory
	
	// Outputs
	output  wire [NB_DIR_PC - 1 : 0]   o_PC_Out   // PC output indicates the address to read from program memory    
 );
 
 // Internal Registers
 reg    [NB_DIR_PC - 1 : 0]   ProgCounter;
 
 	always @(posedge i_clk)
 	begin
		if (i_Reset)
		begin
		      ProgCounter <= {NB_DIR_PC{1'b0}};
		      //$display("PC Reset");
		end 
		else if (i_Halt || i_Stall)
			ProgCounter <= ProgCounter;
		else
		begin
			ProgCounter <= i_PC_In;		
			//$display("Writing PC with %d",i_PC_In);
        end           
	end

	assign o_PC_Out   = ProgCounter;
 
endmodule
