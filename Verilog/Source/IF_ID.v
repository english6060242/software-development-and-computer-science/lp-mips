`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01.06.2022 09:09:35
// Design Name: 
// Module Name: IF_ID
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module IF_ID
#(
    parameter   NB_DATA   = 32
 )
(
    // Inputs
    input   wire                        i_clk,                  // Clock
    input   wire                        i_Reset,                // Reset
    input   wire                        i_Stall,   // Stop pipeline in case of control hazards
    input   wire                        i_Halt,    // Halt processor (instruction halt)
    input   wire    [NB_DATA - 1 : 0]   i_Instruction_IF,
    input   wire    [NB_DATA - 1 : 0]   i_PC_Plus_Four_IF,


    // Outputs
    output  wire    [NB_DATA - 1 : 0]   o_Instruction_ID,
    output  wire    [NB_DATA - 1 : 0]   o_PC_Plus_Four_ID
 );  
 
    reg      [NB_DATA - 1 : 0]   Instruction;
    reg      [NB_DATA - 1 : 0]   PC_Plus_Four;
    
    assign  o_Instruction_ID = Instruction;
    assign  o_PC_Plus_Four_ID = PC_Plus_Four;
    
    always@(posedge i_clk)
    begin
        if(i_Reset)
        begin
            Instruction <= 32'b0;
            Instruction <= {6'b111110, 26'b0}; // Initializing with the initial PC value
            PC_Plus_Four <= 32'b0;
        end
        else if(i_Stall || i_Halt)
        begin
            Instruction <= Instruction;
            PC_Plus_Four <= PC_Plus_Four;
        end
        else
        begin
            Instruction <= i_Instruction_IF;
            PC_Plus_Four <= i_PC_Plus_Four_IF;
        end
    end
    
endmodule
