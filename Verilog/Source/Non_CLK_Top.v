`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01.06.2022 12:37:09
// Design Name: 
// Module Name: Non_CLK_Top
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Non_CLK_Top
#(
    parameter   NB_DATA = 32,
    parameter   NB_UART = 8,    // Number of data bits
    parameter   SB_TICK = 16,
    parameter   NB_CONT = 4,
    parameter   NB_BRCV = 3,
    parameter   M = 14,        // Parameter for Baud Rate Generator
    parameter   W = 8,          // # buffer of W bits
    parameter   OP_BITS = 6,    // Operation bits
    parameter   NB_STAT = 4,    // Number of bits to represent states
    parameter   NB_SEND = 384, // Number of bits to send (table)
    parameter   TC_WORD = 12,    // Number of 32-bit words in the table
    parameter   NB_DIR_PC   = 32,
    parameter   NB_ALUOP    = 4,
    parameter   NB_SHAMT    = 5,
    parameter   NB_OPCODE   = 6,
    parameter   NB_FUNC     = 6,
    parameter   NB_RDIR     = 5,     // Register addressing bits
    parameter   NB_MUX4     = 2,
    parameter   NB_ALLR     = 160,  // Number of bits of registers to send
    parameter   NB_DMEM     = 160    // Number of bits of data memory to send
 )
(
    input   wire                          i_clk,                  // Clock
    input   wire                          i_Reset,                // Reset 
    input   wire                          i_rx,
   
    output  wire                          o_tx    
);
 
    wire                                hlt;
    wire        [NB_SEND  - 1 : 0]      r_table;
    wire                                Prog_Mem_Write;
    wire        [NB_DATA  - 1 : 0]      Prog_Mem_Address;
    wire        [NB_UART  - 1 : 0]      Write_Data;
    wire                                Debug_Stall;
   
Debug_Unit
#(
    .NB_DATA                (NB_DATA),
    .NB_UART                (NB_UART),     // Number of data bits
    .W                      (W),           // # buffer of W bits
    .OP_BITS                (OP_BITS),     // Operation bits
    .NB_STAT                (NB_STAT),     // Number of bits to represent states
    .NB_SEND                (NB_SEND),     // Number of bits to send (table)
    .TC_WORD                (TC_WORD)      // Number of 32-bit words in the table
)
u_Debug_Unit_1
(
    .i_clk                  (i_clk),
    .i_Reset                (i_Reset),
   
    .i_rx                   (i_rx),        

    .i_hlt                  (hlt),
    .i_table                (r_table),  
   
    // Program Memory Outputs
    .o_Prog_Mem_Address     (Prog_Mem_Address),
    .o_Prog_Mem_Byte        (Write_Data[7 : 0]),
    .o_Prog_Mem_Write       (Prog_Mem_Write),
   
    // MIPS Outputs
    .o_Debug_Stall          (Debug_Stall),
   
    // Tx Outputs
    .o_tx                   (o_tx)    
);

MIPS
#(
    .NB_DATA                (NB_DATA),
    .NB_UART                (NB_UART),
    .NB_DIR_PC              (NB_DIR_PC),
    .NB_ALUOP               (NB_ALUOP),
    .NB_SHAMT               (NB_SHAMT),
    .NB_OPCODE              (NB_OPCODE),
    .NB_FUNC                (NB_FUNC),
    .NB_RDIR                (NB_RDIR),     // Register addressing bits
    .NB_MUX4                (NB_MUX4),
    .NB_SEND                (NB_SEND),     // Number of bits to send (table)
    .NB_ALLR                (NB_ALLR),     // Number of bits of registers to send
    .NB_DMEM                (NB_DMEM)      // Number of bits of data memory to send
 )
u_MIPS_1
(
    .i_clk                  (i_clk),
    .i_Reset                (i_Reset),                // Reset
    .i_Stall                (Debug_Stall),  
    .i_Prog_Write_Address   (Prog_Mem_Address),
    .i_Prog_MEM_Data        (Write_Data),
    .i_Prog_MEM_Write       (Prog_Mem_Write),
    // Outputs
    .o_HLT                  (hlt),
    .o_table                (r_table)
 );
   
endmodule
