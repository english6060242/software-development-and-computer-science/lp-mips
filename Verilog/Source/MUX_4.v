`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01.06.2022 09:12:14
// Design Name: 
// Module Name: MUX_4
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module MUX4
#(
    parameter   NB_DATA = 32
 )
 (
    // Inputs
    input   wire    [NB_DATA - 1 : 0]    i_MUX_0,
    input   wire    [NB_DATA - 1 : 0]    i_MUX_1,
    input   wire    [NB_DATA - 1 : 0]    i_MUX_2,
    input   wire    [NB_DATA - 1 : 0]    i_MUX_3,
    input   wire    [1           : 0]    i_MUX_Select,
    // Outputs
    output  wire    [NB_DATA - 1 : 0]    o_MUX
 );
 
 reg [NB_DATA - 1 : 0] MUX;
 
 assign o_MUX = MUX;
 
  always @(i_MUX_0, i_MUX_1, i_MUX_2, i_MUX_3, i_MUX_Select) 
  begin
    case(i_MUX_Select)
        2'b00: MUX = i_MUX_0;
        2'b01: MUX = i_MUX_1;
        2'b10: MUX = i_MUX_2;
        2'b11: MUX = i_MUX_3;
        default: MUX = i_MUX_0;
    endcase
  end

endmodule
