#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "textProcessing.h"
#include "labelList.h"
#include "fillInstruction.h"

#define TAM 1024

int main(int argc, char* argv[]) 
{
  char Instruction[33] = "00000000000000000000000000000000";  
  // Apparently if one uses exactly the needed amount of characters, the string concatenates with the next because there's no space for the null terminator
  char fileName[50] = "Program.txt"; /* should check that argc > 1 */
  char destFileName[50] = "Code.mem"; /* should check that argc > 1 */
  char destFileName2[50] = "CLP.mem";
  char CLP_string[256];
  char CLP_string_Bin[256];

  FILE* file = fopen(fileName, "r"); /* should check the result */
  FILE* destFile = fopen(destFileName, "w"); /* should check the result */
  FILE* destFile2 = fopen(destFileName2, "w"); /* should check the result */
  
  int buffsize = TAM, position = 0;
  int CLP = 0;
  
  char line[256];
  char line2[256];
  char *aux = malloc(buffsize * sizeof(char));
  char **args = malloc(buffsize * sizeof(char*));

  char *aux2 = malloc(buffsize * sizeof(char));
  char **args2 = malloc(buffsize * sizeof(char*));

  struct labelNode * head = NULL;
  head = (struct labelNode *) malloc(sizeof(struct labelNode));
  head->next=NULL;
  head->dir = -1;
  
  if (!aux) 
  {
      fprintf(stderr, "Memory allocation error\n");
      exit(EXIT_FAILURE);
  }

  if (!args) 
  {
      fprintf(stderr, "Memory allocation error\n");
      exit(EXIT_FAILURE);
  }
  
  int j = 0;

  while (fgets(line, sizeof(line), file)) 
  {
    position = 0;
    //printf("Line: %d\n",j);
    separateComments(args2, line, aux2, position, buffsize);
    separateWords(args2, line, aux2, position, buffsize);
    //printf("args2[0]: %s\n",args2[0]);
    if(args2[1] != NULL)
    {
      if(isMnemonic(args2[1]))
      {
        addLabel(head,args2[0],j);
      }
    }
    j++;
  }

  CLP = j; // CLP = Program Line Count

  fclose(file);
  file = fopen(fileName, "r");

  j = 0;
  while (fgets(line, sizeof(line), file)) 
  {
    //args = malloc(buffsize * sizeof(char*));
    position = 0;
    //printf("Line: %d\n",j);
    
    separateComments(args, line, aux, position, buffsize);

    separateWords(args, line, aux, position, buffsize);

    if(args[1] != NULL)
    {
      if(isMnemonic(args[1]))
      {
        // There's a label
        //addLabel(head,args[0],j);
        int l = 0;
        while(args[l] != NULL)
        {
          args[l] = args[l+1];
          l++;
        }
      }
    }

    // If it's a jump instruction, the instruction number referred by the label must be obtained
    if((isEgroup(args[0]) != -1) && (!(isNumeric(args[1]))))  // j, jal
    {
      //printf("%s: ",args[1]);
      getLabelNumber(head,args[1],args[1]);
      //printf("Dir: %s\n",args[1]);
    }

    if((isIgroup(args[0]) != -1) && (!(isNumeric(args[3]))))  // j, jal
    {
      //printf("%s: ",args[1]);
      //getLabelNumber(head,args[3],args[3]);
      
      sprintf(args[3],"%d",(getIntLabelNumber(head,args[3]) - (j+1))); 
      //printf("I-type Dir: %s\n",args[3]);
    }

    fillInstruction(args,Instruction,j,CLP);
    //printf("Inst: %s\n",Instruction);
    fprintf(destFile,"%s",Instruction);
    j++;
  } // This while loop stops when it reaches the end of the file

  sprintf(CLP_string,"%d",j);
  stringToBin32(CLP_string,CLP_string_Bin);
  //fprintf(destFile2,"%d",j);
  fprintf(destFile2,"%s",CLP_string_Bin);

  //printLabelList(head);
  freeList(head);
  free(aux);
  free(args);
  free(aux2);
  free(args2);
  fclose(file);
  fclose(destFile);
  fclose(destFile2);

  return 0;
}