#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// Struct for a node in the label list
struct labelNode {
    char labelText[50];  // Assuming a maximum label text length of 50 characters
    int dir;
    struct labelNode *next;
};

// Function to add a label to the label list
void addLabel(struct labelNode *head, char *text, int labelDir) {
    if (!labelExists(head, text)) {
        struct labelNode *current = head;
        if (head->dir != -1) {
            // List already has elements, add another
            while (current->next != NULL) {
                current = current->next;
            }
            current->next = (struct labelNode *)malloc(sizeof(struct labelNode));
            strcpy(current->next->labelText, text);
            current->next->dir = labelDir;
            current->next->next = NULL;
        } else {
            // First element of the list
            strcpy(current->labelText, text);
            current->dir = labelDir;
            current->next = NULL;
        }
    }
}

// Function to check if a label already exists in the list
int labelExists(struct labelNode *head, char *text) {
    struct labelNode *current = head;
    while (current != NULL) {
        if (!strcmp(current->labelText, text)) {
            return 1;
        }
        current = current->next;
    }
    return 0;
}

// Function to print the list of labels
void printLabelList(struct labelNode *head) {
    struct labelNode *current = head;
    while (current != NULL) {
        printf("dir: %d ; text: %s\n", current->dir, current->labelText);
        current = current->next;
    }
}

// Function to get the number associated with a label
void *getLabelNumber(struct labelNode *head, char *label, char *dest) {
    struct labelNode *current = head;
    if (labelExists(head, label)) {
        while (current != NULL) {
            if (!strcmp(current->labelText, label)) {
                sprintf(dest, "%d", current->dir);
            }
            current = current->next;
        }
    } else {
        printf("Non-existent jump label\n");
        exit(EXIT_FAILURE);
    }
}

// Function to get the integer number associated with a label
int getIntLabelNumber(struct labelNode *head, char *label) {
    struct labelNode *current = head;
    if (labelExists(head, label)) {
        while (current != NULL) {
            if (!strcmp(current->labelText, label)) {
                return current->dir;
            }
            current = current->next;
        }
    } else {
        printf("Non-existent jump label\n");
        return -1;
    }
}

// Function to count the number of nodes in the label list
int LabelListSize(struct labelNode *head) {
    int count = 0;
    struct labelNode *current = head;
    while (current != NULL) {
        count++;
        current = current->next;
    }
    return count;
}

// Function to free the memory allocated for the label list
void freeList(struct labelNode *head) {
    struct labelNode *tmp;
    while (head != NULL) {
        tmp = head;
        head = head->next;
        free(tmp);
    }
}