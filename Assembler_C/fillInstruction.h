#ifndef FILLINSTRUCTION_H    // Begin ifdef guard
#define FILLINSTRUCTION_H_H

    void fillInstruction(char ** args, char *Inst, int j, int CLP);

    int isAgroup(char *mnem);

    int isBgroup(char *mnem);

    int isCgroup(char *mnem);

    int isDgroup(char *mnem);

    int isEgroup(char *mnem);

    int isFgroup(char *mnem);

    int isGgroup(char *mnem);

    int isHgroup(char *mnem);

    int isIgroup(char *mnem);

    int isJgroup(char *mnem);

    int isNOP(char *mnem);

    int isHLT(char *mnem);

    void stringToBin(char *string, char *binRep);

    void stringToBin16(char *string, char *binRep);

    void stringToBin26(char *string, char *binRep);

    void stringToBin32(char *string, char *binRep);
    
    void removeChar(char *string, char toRemove);

#endif // End ifdef guard