#ifndef TEXTPROCESSING_H    // Begin ifdef guard
#define TEXTPROCESSING_H

    void separarComentarios(char **args, char *line, char *aux, int position, int buffsize);

    void separarPalabras(char **args, char *line, char *aux, int position, int buffsize);

    int isMnemonic(char *w);

    int isNumeric(char *string);

#endif // End ifdef guard