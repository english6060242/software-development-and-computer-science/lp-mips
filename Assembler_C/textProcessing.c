#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>  // isdigit()

#define TAM 1024

void separateComments(char **args, char *line, char *aux, int position, int buffsize)
{
  // strtok(char *str, const char *delim) separates str every time it finds something contained in delim  
  aux = strtok(line, ";");
  while(aux != NULL)
  {
    args[position] = aux;
    position++;
    if(position >= buffsize)	// If the number of elements exceeds 1000
    {
      buffsize += TAM;	// 1000 more are added to buffsize
      args = realloc(aux, buffsize * sizeof(char*));	// The memory block of Arguments is enlarged with the new size
      if (!args) 
      {
        fprintf(stderr, "Memory allocation error\n");	// Upon failure: message and exit
        exit(EXIT_FAILURE);
      }
    }
    // aux = strtok(NULL, " \n");	// next word
    aux = strtok(NULL, ";");	// next word
  }
  args[position] = NULL;
}

void separateWords(char **args, char *line, char *aux, int position, int buffsize)
{
  position = 0;
  aux = strtok(args[0], " ,\n");
  while(aux != NULL)
  {
    args[position] = aux;
    position++;
    if(position >= buffsize)	// If the number of elements exceeds 1000
    {
      buffsize += TAM;	// 1000 more are added to buffsize
      args = realloc(aux, buffsize * sizeof(char*));	// The memory block of Arguments is enlarged with the new size
      if (!args) 
      {
        fprintf(stderr, "Memory allocation error\n");	// Upon failure: message and exit
        exit(EXIT_FAILURE);
      }
    }
    aux = strtok(NULL, " ,\n()$");	// next word
  }
  args[position] = NULL;
  /*int i = 0;
  while(args[i] != NULL)
  {
    printf("Argument %d: %s\n", i, args[i]);
    i++;
  }*/
}

int isMnemonic(char *w)
{
  char *Instructions[] = {"addu", "subu", "and", "or", "xor", "nor", "slr", "sllv", "srlv", "srav", "sll", "srl", "sra", "lb", "lh", "lw", "lwu", "lbu", "lhu",
    "sb", "sh", "sw", "lui", "addi", "andi", "ori", "xori", "slti", "beq", "bne", "j", "jal", "jr", "jalr", "nop", "hlt"};

  // printf("Entering %s\n", w);
   
   for(int i = 0; i < 36; i ++)
   {
     if(!strcmp(w, Instructions[i]))  // strcmp returns 0 if the strings are equal
     {
       return 1;
     }
   }
   return 0;
}

int isNumeric(char *string)
{
  for(int i = 0; i < strlen(string); i++)
  {
    if(!isdigit(string[i]))
    {
      return 0;
    }
  }
  return 1;
}